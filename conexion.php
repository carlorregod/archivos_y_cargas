<?php
class Conexion{
    private static function conectar_(){
        $user 					= 'cobranza';
	    $passwd         		= '5Ucr32546!!#$=';
	    $db             		= 'davila_202003';
	    $host           		= '172.16.1.237'; 
	    $port 					= 5432;
	    $strCnx 				= "host=$host port=$port dbname=$db user=$user password=$passwd";
        $con 					= pg_connect ($strCnx) or die ("Error de conexion.". pg_last_error());
        return $con;
    }

    public static function conectar(){
        return self::conectar_();
    }
    //Conexión por medio de PDO, por defecto se conecta a Postgres con el puerto 5432
    private static function conectar_pdo_($db,$host,$user,$password,$protocol,$port){
        //PREVINIENDO ERRORES
        error_reporting(E_ALL);
        ini_set('display_errors', 1);

        $arg = $protocol.":dbname=".$db.";host=".$host.";port=".$port."";

        $usuario = $user;
        $pass = $password;

        //Estableciendo la conexión
        
        $mbd = new PDO($arg, $usuario, $pass) or die('Falló la conexión');
        //Retorno del objeto de la conexión
        return $mbd;
    }

    public static function conectar_pdo($db,$host,$user,$password,$protocol='pgsql',$port=5432){
        return self::conectar_pdo_($db,$host,$user,$password,$protocol='pgsql',$port=5432);
    }
}
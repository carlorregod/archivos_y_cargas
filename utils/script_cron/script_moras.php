<?php 
include_once("lib/PHPMailerAutoload.php");
include_once("lib/class.phpmailer.php");
include_once("mail_config.php");
error_reporting(E_ALL);
ini_set('display_errors', '1');
$data_db=array(
		//"host"=>"172.16.1.237",
		"host"=>"172.16.1.250",
		"port"=>"5432",
		"user"=>"cobranza",
		"pass"=>"cobra2013!!#$",
		//"db"=>"cobra_octubre"
		"db"=>"cobra"
	);
$con_pg	= pg_connect("host=$data_db[host] port=$data_db[port] dbname=$data_db[db] user=$data_db[user] password=$data_db[pass]") or die ("Error de conexion2.". pg_last_error());
function arreglo_columna($arr,$field){
	$col=array();
	for($i=0;$i<count($arr);$i++){
		$col[]=$field;
	}
	if(!function_exists("column")){
		function column($a,$f){
			return $a[$f];
		}
	}
	return array_map("column",$arr,$col);
}
function _cp_($emp_id,$cond){
	global $con_pg;
	$where="";
	switch ($cond) {
		case 1:
			$where="and pag_fechacompromiso=to_date('".date("Y-m-d")."','YYYY MM DD')";
			break;
		case 2:
			$where="and pag_fechacompromiso<to_date('".date("Y-m-d")."','YYYY MM DD')";
			break;
	}
	
	$sql="SELECT 
				pag_fechacompromiso 
			from pagos p
			where p.pag_origen!=1 
			and p.empresa_id=$emp_id $where";
	$result = pg_query($con_pg, $sql);
	$data_cp=array();
	while($row=pg_fetch_row($result,null, PGSQL_ASSOC)){
		$data_cp[]=$row;
	}
	return $data_cp;
}
function reclamos($emp_id){
	global $con_pg;
	$sql="SELECT empresa_id,id from reclamos where empresa_id=$emp_id";
	$result = pg_query($con_pg, $sql);
	$data=array();
	while($row=pg_fetch_row($result,null, PGSQL_ASSOC)){
		$data[]=$row;
	}
	return $data;
}
function otroCompromiso($emp_id){
	$sql="select id,are_fechacompromiso from detalles d join contactos c on d.contacto_id=c.id join empresas e on c.empresa_id=e.id where are_fechacompromiso>to_date('".date("Y-m-d")."','YYYY MM DD') and e.id=$emp_id;";
	$result = pg_query($con_pg, $sql);
	$data=array();
	while($row=pg_fetch_row($result,null, PGSQL_ASSOC)){
		$data[]=$row;
	}
	return $data;
}
function getNumValid($emp_id=null,$rut=null){
	global $con_pg;
	$sql_moras="SELECT 
					con_fono as con_telefono 
					FROM contactos 
					where 
						empresa_id=$emp_id 
						and con_activo=1 
						and con_fono not in ('','0') 
				union 
				select 
					con_celular as con_telefono 
					from contactos  
					WHERE 
						empresa_id=$emp_id 
						and con_celular not in ('','0')  
						and con_activo=1 
				group by con_fono,con_celular;";
	$query_moras = pg_query($con_pg, $sql_moras);
	$numero=array();
	$numero2=array();
	$xnum=array();
	$xnum2=array();
	$exp_cel ='/^[09|08|06|07][0-9]{8}$/';
	$exp_fono='/^[0-9]{2,3}-? ?[0-9]{6,7}$/';

	while($contacto=pg_fetch_row($query_moras,null, PGSQL_ASSOC)){
		preg_match($exp_fono, $contacto["con_telefono"], $coincidencias, PREG_OFFSET_CAPTURE);
		if(isset($coincidencias[0][0])){
			$l_n=strlen($coincidencias[0][0]);
			$clean_n=intval(str_replace(array("-"," "), "", $coincidencias[0][0]));
			if($l_n>=8&&$l_n<=10){
				$array  = array_map('intval', str_split($clean_n));
				$numero[]=in_array($array[0],array(9,8,7,6))?(count($array)==8?"09".$clean_n:"0".$clean_n):$clean_n;
			}else{
				$xnum[]=$clean_n;
			}
		}else{
			$xnum[]="Vacio";
		}
		preg_match($exp_cel, $contacto["con_telefono"], $coincidencias, PREG_OFFSET_CAPTURE);
		if(isset($coincidencias[0][0])){
			$num=intval($coincidencias[0][0]);
			$l_n=strlen($num);
			if($l_n>=8&&$l_n<=10){
				$numero2[]=$l_n==8?"09".$num:($l_n==9?"0".$num:$num);
			}else{
				$xnum2[]=$num;
			}
		}else{
			$xnum2[]="Vacio";
		}

	}
	$count["vacios"]=0;
	$count["malformat"]=0;
	foreach ($xnum as $value) {
		if($value=="Vacio"){
			$count["vacios"]++;
		}else{
			$count["malformat"]++;
		}
	}
	foreach ($xnum2 as $value) {
		if($value=="Vacio"){
			$count["vacios"]++;
		}else{
			$count["malformat"]++;
		}
	}
	$string="El rut empresa: $rut, los contactos no tiene formato de numero de fono/celular (total: $count[malformat]) o estan vacios (total: $count[vacios]).";
	$in_log="insert into log_contactos values (default,'$string',now()) RETURNING id;";
	$query_log= pg_query($con_pg, $in_log);
	return implode("-", $numero).(count($numero)>0&&count($numero2)>0?"-":"").implode("-", $numero2);
}
function getUsers($perfil=null,$parent=null,$limit=null){
	global $con_pg;
	$_us=array();
	$nulos=array("",null," ");
	if(!in_array($perfil,$nulos)&&!in_array($parent, $nulos)){
		$sql_user="SELECT username,id,agente_id from users where perfil_id=$perfil and parent_id=$parent and usu_activo=1 ".($limit!=null?" limit $limit":"").";";
		$query_user = pg_query($con_pg, $sql_user);
		while ($users=pg_fetch_row($query_user,null, PGSQL_ASSOC) ) {
			$_us[]=$users;
		}
	}else{
		$_us=null;
	}

	return $_us;
}
function getFacturas($user_id,$limit=null){
	global $con_pg;
	$query_carga = pg_query($con_pg, "SELECT max(id) as id FROM cargas");
	$carga = pg_fetch_row($query_carga,null, PGSQL_ASSOC);
	$fecha_hoy=date("Y-m-d");
	$sql_emp="SELECT 
				e.emp_rut,
				f.total,
				e.id,
				f.total_provcocr
			from empresas e 
			left join (SELECT 
							sum(mor_importe2) as total,
							sum(mor_provco+mor_procr) as total_provcocr,
							empresa_id,
							carga_id
						from facturasmoras
						where mor_activo=1
						and user_id=$user_id
						and mor_fecvencimiento<='$fecha_hoy'
						and mor_tamoanho!='VIGENTE'
						group by empresa_id,carga_id ) f on e.id=f.empresa_id 
			where e.emp_activo=1 
			and e.user_id=$user_id
			and f.total is not null 
			and f.total != 0
			and f.carga_id=$carga[id]
			order by f.total desc,f.total_provcocr desc 
			".($limit!=null?" limit $limit":"").";";//and u.parent_id=70 id isamel cambiar al de mario

	$query_emp = pg_query($con_pg, $sql_emp);
	$data=array();
	while ($emp = pg_fetch_row($query_emp,null, PGSQL_ASSOC)) {
		$data[]=$emp;
	}
	return $data;
}
function newDoc($user=null,$data){
	$path=null;
	if($user!=null){
		$path="/var/www/html/cobra/script_cron/csv/".date("Ymd")."_$user.csv";
		//$path="script_cron/csv/".date("Ymd")."_$user.csv";
		$stringcompleto='"telefono","rut"'."\r\n".implode("\r\n", arreglo_columna($data,"string"));
		if (!$handle = fopen($path, "w")) {  
		    echo "No se pudo abrir el archivo ".date("Y-m-d H:i:s")."\n";  
		    exit;  
		}  
		if (fwrite($handle, utf8_decode($stringcompleto)) === FALSE) {  
		    echo "No se pudo modificar archivo ".date("Y-m-d H:i:s")."\n";  
		    exit;  
		}  
		fclose($handle);
	}
	return $path;  
}

function init($data,$datamail){
	$realizado=false;
	$datapath=array();
	$dataerror=array();
	foreach ($data as $users) {
		$datamoras=array();
		foreach (getFacturas($users["id"]) as $emp) {
			$cant_cp1=count(_cp_($emp["id"],1));
			$cant_cp2=count(_cp_($emp["id"],2));
			$cant_rc=count(reclamos($emp["id"]));
			if(!$cant_cp1>0&&!$cant_rc>0){
				$tel=getNumValid($emp["id"],$emp["emp_rut"]);
				if(strlen($tel)>0){
					$datamoras[]=array(
									"emp_rut"=>'"'.$emp["emp_rut"].'"',
									"mora_total"=>$emp["total"],
									"mora_vcocr"=>$emp["total_provcocr"],
									"empresa_id"=>$emp["id"],
									"telefono"=>$tel,
									"string"=>$tel.",".'"'.$emp["emp_rut"].'"'
									);
				}else{
					$dataerror[]=array(
									"emp_rut"=>'"'.$emp["emp_rut"].'"',
									"mora_total"=>$emp["total"],
									"mora_vcocr"=>$emp["total_provcocr"],
									"empresa_id"=>$emp["id"],
									"telefono"=>$tel,
									"string"=>$tel.",".'"'.$emp["emp_rut"].'"',
									"estado"=>"No tiene numero de contacto"
									);
				}
			}elseif(!$cant_cp2>0&&!$cant_rc>0){
				$tel=getNumValid($emp["id"],$emp["emp_rut"]);
				if(strlen($tel)>0){
					$datamoras[]=array(
									"emp_rut"=>'"'.$emp["emp_rut"].'"',
									"mora_total"=>$emp["total"],
									"mora_vcocr"=>$emp["total_provcocr"],
									"empresa_id"=>$emp["id"],
									"telefono"=>$tel,
									"string"=>$tel.",".'"'.$emp["emp_rut"].'"'
									);
				}else{
					$dataerror[]=array(
									"emp_rut"=>'"'.$emp["emp_rut"].'"',
									"mora_total"=>$emp["total"],
									"mora_vcocr"=>$emp["total_provcocr"],
									"empresa_id"=>$emp["id"],
									"telefono"=>$tel,
									"string"=>$tel.",".'"'.$emp["emp_rut"].'"',
									"estado"=>"No tiene numero de contacto"
									);
				}
			}else{
				$s_cp=($cant_cp1+$cant_cp2)>0?"compromiso de pago":"";
				$s_rc=$cant_rc>0?"reclamo":"";
				$dataerror[]=array(
									"emp_rut"=>'"'.$emp["emp_rut"].'"',
									"mora_total"=>$emp["total"],
									"mora_vcocr"=>$emp["total_provcocr"],
									"empresa_id"=>$emp["id"],
									"telefono"=>$tel,
									"string"=>$tel.",".'"'.$emp["emp_rut"].'"',
									"estado"=>"Tiene ".$s_cp.(strlen($s_cp)>0&&strlen($s_rc)>0?" y ":"").$s_rc." asociado"
									);
			}
		}
		if(count($datamoras)>0){
			$datapath[]=array("path"=>newDoc($users["username"],$datamoras),"agent"=>$users["agente_id"]);
			$realizado=true;
		}
	}
	if($realizado){
		$estado=sendingFiles($datamail,$datapath);
		if(count($dataerror)>0){
			$new_estado=sendingFiles($datamail,null,$dataerror);
		}
		if($estado==true){
			echo "finalizado";
		}
	}
}

function sendingFiles($datamail,$path=null,$noEmp=null){
	global $gmail;
	$realizado=false;
	if(count($datamail)>0){
		$correo = new PHPMailer();
		$correo->IsSMTP();
		$correo->SMTPAuth = true;
		$correo->SMTPSecure = '';
		$correo->Host = $gmail["host"];
		$correo->Port = $gmail["port"];
		$correo->Username = $gmail["username"];
		$correo->Password = $gmail["password"];
		$correo->SetFrom($gmail["username"], "Cobranza");
		$correo->CharSet = 'UTF-8';
		//$correo->AddReplyTo("micuenta@gmail.com","Mi Codigo PHP");
		foreach ($datamail as $id => $cuentas) {
			if($noEmp==null){
				$correo->AddAddress($cuentas["mail"], $cuentas["nombre"]);
			}else{
				if(in_array($cuentas["mail"],array("johanm.fernandez@itcs.cl","carmen.aguirre@2080.cl"))){
					$correo->AddAddress($cuentas["mail"], $cuentas["nombre"]);
				}
			}
		}
		$cuerpo_mail=null;
		if(count($path)>0){
			foreach ($path as $archivo) {
				$divdata=explode("_", $archivo["path"]);
				$correo->AddAttachment($archivo["path"],date("Ymd")."_$divdata[2]");
			}
			$correo->Subject = "TCH: Archivos de carga para generar campaña para servicio BVI (".date("d-m-Y").")";
			$cuerpo_mail="Estimado Arturo:<br>";
			$cuerpo_mail.="Solicitamos por favor que se genere las campañas de hoy (".date("d-m-Y").") asociadas a los archivos";
			$cuerpo_mail.=" adjuntos para el personal BVI cobranza.";
			$cuerpo_mail.=" Es una campaña por ejecutivo, sin otro particular.<br>Atte.<br>Administrador Cobra 2080.";
		}else{
			if(count($noEmp)>0){
				$correo->Subject = "TCH: Empresas no consideradas en campaña para servicio BVI (".date("d-m-Y").")";
				$cuerpo_mail="Estimado Carmen:<br>";
				$cuerpo_mail.="Las siguientes empresas no estan dentro de la campaña debido a que tienen reclamos, compromisos de pagos asociado y tambien empresas el cual sus numeros de contactos no se encuentran bien definido.";
				$cuerpo_mail.=" en el sistema para hoy (".date("d-m-Y").").<br>";
				$cuerpo_mail.="<table border='1'><tr><td>Rut</td><td>Total Importe</td><td>Total provision</td><td>Estado</td></tr>";
				$nonum="";
				$sicprc="";
				foreach ($noEmp as $empresas) {
					$cuerpo_mail.="<tr><td>$empresas[emp_rut]</td><td>$empresas[mora_total]</td><td>$empresas[mora_vcocr]</td><td>$empresas[estado]</td></tr>";
				}
				$cuerpo_mail.="</table>";
			}
			
		}
		
		$correo->MsgHTML($cuerpo_mail);
		if(!$correo->Send()) {
			echo "Hubo un error: " . $correo->ErrorInfo;
		}else{
			if(count($path)>0){
				foreach ($path as $archivo) {
					unlink($archivo["path"]);
				}
			}
			$realizado=true;

		}
	}
	return $realizado;
}
$datamail=array(
		array("nombre"=>"Johanm","mail"=>"johanm.fernandez@itcs.cl"),
		array("nombre"=>"Carmen","mail"=>"carmen.aguirre@2080.cl"),
		array("nombre"=>"Mario","mail"=>"mario.castillo@2080.cl"),
		array("nombre"=>"Arturo","mail"=>"ams@redcol.cl")
	);
init(getUsers(27,721),$datamail);
?>
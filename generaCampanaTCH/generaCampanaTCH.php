<?php

	ini_set("memory_limit","16000M");
	date_default_timezone_set('America/Santiago');
	define("LOG_SI","1");

	//************************************************************************************************************************************************************
	//          Funciones, clase Biblioteca, métodos estáticos
	//************************************************************************************************************************************************************

	require_once('../biblioteca.php');
	require_once('../conexion.php');
	
	//****************************************************************************
	//                   Carpetas de proceso
	//****************************************************************************
	

	$path_proyecto 			= "/var/www/html/archivos_y_cargas/";
	$path_base				= $path_proyecto;

	$path_base_log_carga	= $path_base."generaCampanaTCH/";
	$path_priorizacion      = $path_base."app/tmp/priorizacion/";
	$path 					= $path_base;
	$path_in 				= $path_base_log_carga."input/";
	$path_proceso 			= $path_base_log_carga."proceso/";
	$path_out				= $path_base_log_carga."out/";
	$path_logs				= $path_base_log_carga."logs/";
	$path_noprocesados      = $path_base_log_carga."no_procesado/";	
	$activo					= 1;
	$suf_in					= "_in";
	$suf_proceso			= "_pro";
	$suf_out				= "_out";
	$archivo_datos          = "";




	//****************************************************************************
	//                   LOG4PHP
	//****************************************************************************
	define('LOG4PHP_DIR', $path_base.'utils/log4php-2.3.0/src/main/php'); 
	define('LOG4PHP_CONFIGURATION',$path_base_log_carga.'config.xml');
	require_once(LOG4PHP_DIR.'/Logger.php'); 
	Logger::configure(LOG4PHP_CONFIGURATION);
	$logger = Logger::getLogger('loggerCarga');
	$logger->debug("Incio log4php");


	//**********************************************************************************************************************
	// 												EJECUCION PROCESO
	//**********************************************************************************************************************
	//****************************************************************************************************
	//                   Chequear si existe archivo de carga antes de ejecutar todo el proceso
	//****************************************************************************************************
	$directorio	 		= opendir($path_in);
	$arr_archivo_csv 	= array();
	$file_mdb 			= "";
	$sw_ok 				= 0;

	$logger->debug("Se ejecuta Crontab de generar campaña TCH");
	$logger->debug("Chequeando si en la carpeta INPUT existe algun archivo de carga para procesar");
	while ($archivo = readdir($directorio)){
		if($archivo!='.' and $archivo!='..'){ 
			$name_archivo_original=$archivo;
			$arr_archivo=explode(".", $name_archivo_original);
			if($arr_archivo[1]=="xlsx" || $arr_archivo[1]=="xls"){
				$logger->debug("Nombre Original del archivo a procesar:".$name_archivo_original);
				$file_mdb=$name_archivo_original;
				$sw_ok=1;
				break;
			} else {
				$sw_ok=0;
			}
		}	
	}
	$logger->debug($sw_ok);
	
	//****************************************************************************************************
	//                   Como existe el archivo se debe mover a la carpeta proceso
	//****************************************************************************************************
	$sw_ok_proceso=0;
	if($sw_ok==1){
		$logger->debug("Moviendo el Archivo a la carpeta de INPUT a PROCESO");
		$sw_ok_proceso = 1;	
		$directorio_proceso = opendir($path_proceso);
		//--> Moviendo el archivo de la carpeta INPUT a PROCESO
		try{
			exec('cd '.$path_base_log_carga.'input/; mv *.xlsx '.$path_base_log_carga.'proceso/; ');
		}
		catch(Exception $e){
			$logger->debug("Problemas, no se mueve archivo a procesado");
			
		} 
	} else {
		$sw_ok_proceso = 0;
		$logger->debug("No existe Archivo a procesar");
	}

	$sw_ok_out=0;
	$fecha_volcado="";
	if($sw_ok_proceso==1){
		$logger->debug("Comienzo de la carga de datos como tal");
		
		$sw_ok_out = 1;	
		$logger->debug("Comienzo de la carga de datos como tal pero en Proceso");
		$fecha_hoy 		= date("Ymd");
		$fecha 			= date("Ymd");
		$directorio	 	= opendir($path_proceso);
		$arr_archivo_csv= array();
		$file_mdb 		= "";
		$sw_ok 			= 0;
		$logger->debug("Archivos listado leido: $archivo");

		while ($archivo = readdir($directorio)){
			if($archivo!='.' and $archivo!='..'){ 
				$name_archivo_original=$archivo;
				$arr_archivo=explode(".", $name_archivo_original);

				if($arr_archivo[1]==="xlsx" || $arr_archivo[1]==="xls"){
					//echo $arr_archivo;
					$file_mdb=$name_archivo_original;
					$archivo_datos = $file_mdb;
					$sw_ok=1;
					//echo "paso por acá";
					//echo $sw_ok;
				} else {
					//echo $archivo;
					//echo "no debería pasar por acá";
					$sw_ok=0;
				}
			}	
		}

			
		$logger->debug($sw_ok);
		$logger->debug($path_proceso); //Muestra efectivamente la ruta de proceso
			

		//--> Paso 1: Leyendo las lineas del archivo
		//	/--> Paso 1.1 Obteniendo las lineas del archivo	
			$file_dat = $path_proceso.$file_mdb;
			
		//	--> Paso 1.2 verificando el leer archivo
			$archivo 		= $file_dat;
			$inputFileType 	= PHPExcel_IOFactory::identify($archivo);
			$objReader 		= PHPExcel_IOFactory::createReader($inputFileType);
			$objPHPExcel 	= $objReader->load($archivo);
			$sheet 			= $objPHPExcel->getSheet(0); 
			$highestRow 	= $sheet->getHighestRow(); 
			$highestColumn 	= $sheet->getHighestColumn();

			$arreglo_lectura=array();
			//Recorriendo el archivo excel y escribiendo el array
			 for ($row = 2; $row <= $highestRow; $row++){  
				 //Asegurando que el rut no esté como cadena vacía o nulo
				 $rut = utf8_encode(trim($sheet->getCell("E".$row)->getValue()));
				 if($rut != '' || $rut != null){
				//	--> Paso 1.3 Obteniendo los datos por columnas
					$logger->debug("Procesando line:".$row);
					array_push($arreglo_lectura,array(
						
							'rut'	 		=>  $rut,															//--> RUT
							'Nombre' 		=>  utf8_encode(trim($sheet->getCell("F".$row)->getValue())),		//--> NOMBRE
							'NumPerdido' 	=>  utf8_encode(trim($sheet->getCell("A".$row)->getValue())),		//--> FONO1
							'Movil1'	 	=>  utf8_encode(trim($sheet->getCell("S".$row)->getValue())),		//--> FONO1
							'Movil2'	 	=>  utf8_encode(trim($sheet->getCell("T".$row)->getValue())),		//--> FONO2
							'Movil3'	 	=>  utf8_encode(trim($sheet->getCell("U".$row)->getValue())),		//--> FONO3
							'Fijo1'	 		=>  utf8_encode(trim($sheet->getCell("V".$row)->getValue())),		//--> FONO4
							'Fijo2'	 		=>  utf8_encode(trim($sheet->getCell("W".$row)->getValue())),		//--> FONO5
							'Fijo3'	 		=>  utf8_encode(trim($sheet->getCell("X".$row)->getValue())),		//--> FONO6
							'Domicilio'		=>  utf8_encode(trim($sheet->getCell("M".$row)->getValue())),		//--> DOMICILIO, POSIBLEMENTE LO PIDAN
							'Numero'		=>  utf8_encode(trim($sheet->getCell("N".$row)->getValue())),		//--> NUM CALLE, POSIBLEMENTE LO PIDAN
							'Num_depto'		=>  utf8_encode(trim($sheet->getCell("O".$row)->getValue())),		//--> NUM DEPARTAMENTO, CELDA VACIA SI NO TIENE, POSIBLEMENTE LO PIDAN
							'Comuna'		=>  utf8_encode(trim($sheet->getCell("R".$row)->getValue()))		//--> COMUNA, POSIBLEMENTE LO PIDAN

						)
					);

				} else {
					$logger->debug("Línea:".$row." ha sido saltada, revisar caso");
				}
		
			}
			//$arreglo_lectura contiene todos los ruts y números de teléfono
			
			//Para la query, se necesita una nueva colección de sólo arrays
			$array_rut = array();
			foreach($arreglo_lectura as $ruts){
				array_push($array_rut,$ruts['rut']);
			}
			//Limpiando ruts duplicados
			$array_rut = array_unique($array_rut);

			//Formando un string para la ejecución de la query
			$in_rut = '(';
			foreach($array_rut as $ruts){
				$in_rut .= "'".$ruts."', ";
			}
			$in_rut = substr($in_rut,0,-2);
			$in_rut .= ")";

			//Ahora que se ha procesado el archivo excel, se mueve el archivo ok.txt 
			exec('cd '.$path_base_log_carga.'input/; mv *.txt '.$path_base_log_carga.'proceso/; ');
			$logger->debug("Proceso de lectura de archivo terminado, se mueve ela rchivo de txt a proceso");
			//->Paso 2: Efectuando la query que traerá todos los resultados de números y afines.
			
			//Esta query general se dejará comentada. Ralentiza demasiado la búsqueda.
			/* $query_buscar = "select rut, fono, count(fono) from ((select rut_id as rut, numero_fono as fono, 'davila' as empresa from dav_telefonia) 
			union 
			(select rut_titular_cuenta as rut, nro_elastix as fono, 'claro' as empresa from dav_telefonia_claro)  
			union (
			select rut as rut, numero_fono as fono, 'santander' as empresa from dav_telefonia_santander)
			union 
			(select num_ident as rut, tef_cliente1 as fono, 'TCH' as empresa from ge_clientes)
			union
			(select num_ident as rut, tef_cliente2 as fono, 'TCH' as empresa from ge_clientes)
			union
			(select num_ident as rut,CAST(num_celular AS character varying) as fono, 'TCH' as empresa from ge_clientes)
			) query 
			where fono ~ '^[0]{1}[9]{1}[5-9]{1}[0-9]{7}$|^[2]{1}[2]{1}[1-9]{1}[0-9]{6}$|^[3-7]{1}[1-8]{1}[1-9]{1}[0-9]{6}$'  
			and rut in".$in_rut." group by rut, fono order by rut, fono "; */
			
			//->Paso 2.1: Preparar las querys
			$logger->debug("Ejecución de las querys");
			/* Query base de datos 1: Claro */
			$query_busqueda_claro = "select rut_titular_cuenta as rut, nro_elastix as fono, 'claro' as empresa, 
			count(nro_elastix) from dav_telefonia_claro where nro_elastix ~ '^[0]{1}[9]{1}[5-9]{1}[0-9]{7}$|^[2]{1}[2]{1}[1-9]{1}[0-9]{6}$|^[3-7]{1}[1-8]{1}[1-9]{1}[0-9]{6}$|[9]{1}[5-9]{1}[0-9]{6}' 
			and rut_titular_cuenta in".$in_rut." group by rut_titular_cuenta, fono order by rut_titular_cuenta, 
			nro_elastix ";
			/* Query base de datos 1: Santander */
			$query_busqueda_santander = "select rut, numero_fono as fono, 'santander' as empresa, 
			count(numero_fono) from dav_telefonia_santander where numero_fono ~ '^[0]{1}[9]{1}[5-9]{1}[0-9]{7}$|^[2]{1}[2]{1}[1-9]{1}[0-9]{6}$|^[3-7]{1}[1-8]{1}[1-9]{1}[0-9]{6}$|[9]{1}[5-9]{1}[0-9]{6}' 
			and rut in".$in_rut." group by rut, numero_fono order by rut, numero_fono ";
			/* Query base de datos davila */
			$query_busqueda_davila="select rut_id as rut, numero_fono as fono, 'davila' as empresa, 
			count(numero_fono) from dav_telefonia where numero_fono ~ '^[0]{1}[9]{1}[5-9]{1}[0-9]{7}$|^[2]{1}[2]{1}[1-9]{1}[0-9]{6}$|^[3-7]{1}[1-8]{1}[1-9]{1}[0-9]{6}$|[9]{1}[5-9]{1}[0-9]{6}' 
			and rut_id in".$in_rut." group by rut_id, numero_fono order by rut_id, numero_fono ";
			/* Querys localizadoras para bd ge_clientes, de TCH, divididas en 3 para garantizar valores únicos y reales */
			$query_busqueda_tch1="select num_ident as rut, tef_cliente1 as fono1, 'TCH1' as empresa, 
			count(tef_cliente1) from ge_clientes where tef_cliente1 ~ '^[0]{1}[9]{1}[5-9]{1}[0-9]{7}$|^[2]{1}[2]{1}[1-9]{1}[0-9]{6}$|^[3-7]{1}[1-8]{1}[1-9]{1}[0-9]{6}$|[9]{1}[5-9]{1}[0-9]{6}' 
			and num_ident in".$in_rut." group by num_ident, tef_cliente1, tef_cliente2, num_celular order 
			by num_ident, tef_cliente1 ";
			
			$query_busqueda_tch2="select num_ident as rut, tef_cliente2 as fono2, 'TCH2' as empresa, 
			count(num_celular) from ge_clientes WHERE tef_cliente2 ~ '^[0]{1}[9]{1}[5-9]{1}[0-9]{7}$|^[2]{1}[2]{1}[1-9]{1}[0-9]{6}$|^[3-7]{1}[1-8]{1}[1-9]{1}[0-9]{6}$|[9]{1}[5-9]{1}[0-9]{6}' 
			and num_ident in".$in_rut." group by num_ident, tef_cliente2 order by num_ident, tef_cliente2 ";
			
			$query_busqueda_tch3="select num_ident as rut, num_celular as fono3, 'TCH3' as empresa, 
			count(num_celular) from ge_clientes where cast(num_celular AS CHARACTER VARYING) ~ '^[0]{1}[9]{1}[5-9]{1}[0-9]{7}$|^[2]{1}[2]{1}[1-9]{1}[0-9]{6}$|^[3-7]{1}[1-8]{1}[1-9]{1}[0-9]{6}$|[9]{1}[5-9]{1}[0-9]{6}' 
			and num_ident in".$in_rut." group by num_ident, num_celular order by num_ident, num_celular ";
			
			//->Paso 2.2: Ejecutando las querys. Esto puede demorar alrededor de 1 minuto por el gran volumen de datos
			$coleccion_santander 	= Biblioteca::queryArray($query_busqueda_santander);
			$coleccion_claro 		= Biblioteca::queryArray($query_busqueda_claro);
			$coleccion_davila 		= Biblioteca::queryArray($query_busqueda_davila);
			$coleccion_tch1 		= Biblioteca::queryArray($query_busqueda_tch1);
			$coleccion_tch2 		= Biblioteca::queryArray($query_busqueda_tch2);
			$coleccion_tch3 		= Biblioteca::queryArray($query_busqueda_tch3);

			//Acà las respuestas esperadas son el par rut+telèfono. Se han omitido nùmeros desconocido o 
			//Con formatos no telefònicos. Esto trae como resultado: rut - fono -empresa. Con esto en cuenta se formará la colección
			//completa.
			$logger->debug("Querys exitosas. Todas como arreglos, Unión de arrays en una sola.");
			
			$coleccion_agenda = array();
			$prioridad	=	0;

			//Paso 3: Extrayendo los números y dejándolos dentro de $coleccion_agenda. Para ello se harán varios foreach.
			unset($ruts); 
			$param_fono = 	'Movil1';
			$empresa	=	'TCH-BBDD original "contacto_movil_1"';
			$prioridad	+=	1;
			$coleccion_agenda = Biblioteca::completaArrayTelefonico($coleccion_agenda, $arreglo_lectura, $param_fono, $empresa, $prioridad);
			
			$param_fono = 	'Movil2';
			$empresa	=	'TCH-BBDD original "contacto_movil_2"';
			$prioridad	+=	1;
			$coleccion_agenda = Biblioteca::completaArrayTelefonico($coleccion_agenda, $arreglo_lectura, $param_fono, $empresa, $prioridad);
			
			$param_fono = 	'Movil3';
			$empresa	=	'TCH-BBDD original "contacto_movil_3"';
			$prioridad	+=	1;
			$coleccion_agenda = Biblioteca::completaArrayTelefonico($coleccion_agenda, $arreglo_lectura, $param_fono, $empresa, $prioridad);
			
			$param_fono = 	'Fijo1';
			$empresa	=	'TCH-BBDD original "contacto_movil_4"';
			$prioridad	+=	1;
			$coleccion_agenda = Biblioteca::completaArrayTelefonico($coleccion_agenda, $arreglo_lectura, $param_fono, $empresa, $prioridad);
			
			$param_fono = 	'Fijo2';
			$empresa	=	'TCH-BBDD original "contacto_movil_5"';
			$prioridad	+=	1;
			$coleccion_agenda = Biblioteca::completaArrayTelefonico($coleccion_agenda, $arreglo_lectura, $param_fono, $empresa, $prioridad);
			
			$param_fono = 	'Fijo3';
			$empresa	=	'TCH-BBDD original "contacto_movil_6"';
			$prioridad	+=	1;
			$coleccion_agenda = Biblioteca::completaArrayTelefonico($coleccion_agenda, $arreglo_lectura, $param_fono, $empresa, $prioridad);

			if($coleccion_tch1){
				//Ahora se efectuará la inserción a ese array desde las querys
				$param_fono = 	'fono1';
				$empresa	=	'TCH-1';
				$prioridad	+=	1;
				$coleccion_agenda = Biblioteca::completaArrayTelefonico($coleccion_agenda, $coleccion_tch1, $param_fono, $empresa, $prioridad);
			}

			if($coleccion_tch2){
				$param_fono = 	'fono2';
				$empresa	=	'TCH-2';
				$prioridad	+=	1;
				$coleccion_agenda = Biblioteca::completaArrayTelefonico($coleccion_agenda, $coleccion_tch2, $param_fono, $empresa, $prioridad);
			}
			
			if($coleccion_tch3){
				$param_fono = 	'fono3';
				$empresa	=	'TCH-3';
				$prioridad	+=	1;
				$coleccion_agenda = Biblioteca::completaArrayTelefonico($coleccion_agenda, $coleccion_tch3, $param_fono, $empresa, $prioridad);
			}
			
			if($coleccion_claro){
				$param_fono = 	'fono';
				$empresa	=	'claro';
				$prioridad	+=	1;
				$coleccion_agenda = Biblioteca::completaArrayTelefonico($coleccion_agenda, $coleccion_claro, $param_fono, $empresa, $prioridad);
			}
			
			if($coleccion_santander){
				$param_fono = 	'fono';
				$empresa	=	'santander';
				$prioridad	+=	1;
				$coleccion_agenda = Biblioteca::completaArrayTelefonico($coleccion_agenda, $coleccion_santander, $param_fono, $empresa, $prioridad);
			}
			
			if($coleccion_davila){
				$param_fono = 	'fono';
				$empresa	=	'davila';
				$prioridad	+=	1;
				$coleccion_agenda = Biblioteca::completaArrayTelefonico($coleccion_agenda, $coleccion_davila, $param_fono, $empresa, $prioridad);
			}
			
			
			//Esta última, es para aquellos casos de almacenar los números con problemas como última prioridad
			$param_fono = 	'NumPerdido';
			$empresa	=	'TCH-BBDD original "contacto_movil_problema"';
			$prioridad	+=	1;
			$coleccion_agenda = Biblioteca::completaArrayTelefonico($coleccion_agenda, $arreglo_lectura, $param_fono, $empresa, $prioridad);
			
			
			$logger->debug("Unión de arrays y limpieza de teléfonos vacíos terminada.");
			$logger->debug("QUERYS Y ARRAYS FINALIZADOS. INICIO DE GENERACIÓN CSV DE CAMPAÑA.");
			
			//->Paso 3: Generación CSV para campaña
			//->Paso 3.1: Concatenación de valores según formatos csv
			//$coleccion_agenda ya ahora contiene todos los elementos. 
			$texto_csv = Biblioteca::txtCSV($coleccion_agenda, 'rut', $arreglo_lectura);
			if($texto_csv){ //Método da false si no logra concatenar todo
				//->Paso 3.2 :Generando una carpeta y archivos
				$logger->debug("Verificando y creando carpetas y archivo para genera rel csv ");
				if(!is_dir($path_base_log_carga."campanas")){
					mkdir($path_base_log_carga."campanas", 0777);
				}
				$fecha=date('Ymd');
				if(!is_dir($path_base_log_carga."campanas/".$fecha)){
					mkdir($path_base_log_carga."campanas/".$fecha, 0777);
				}

				$hora = date('hms');
				$dat_file     = $fecha."-".$hora."_carteraTCH.csv";   //-->Nombre del Fichero
				$path_cartera = $path_base_log_carga."campanas/".$fecha."/".$dat_file;
				
				//->Paso 3.3: Escribiendo y generando el archivo
				$fp = fopen($path_cartera,"w");		//No hay problema, el sistema generará el archivo automáticamente			
				fwrite($fp, $texto_csv);
				fclose($fp);
				//->Paso 3.4: Éxito. Envío de correo a destinatarios
				echo "Archivo generado en ".$path_base_log_carga."campanas/".$fecha."/".$dat_file;
				$logger->debug("Archivo generado en ".$path_base_log_carga."campanas/".$fecha."/".$dat_file." ");
				$logger->debug("Envìo por correo electrónico el archivo generado");
				
				//Colección de destinatarios
				$destinatarios =array(
					"carlos.orrego@staff.2080.cl"
					/* "carmen.aguirre@2080.cl",
					"marco.soruco@2080.cl",
					"rodrigo.correa@2080.cl",
					"jma@redcol.cl", */
				);
				//Coleccion de rutas de archivos. En este caso sólo es un archivo
				$path_adjuntos = array(
					$path_base_log_carga."campanas/".$fecha."/".$dat_file
				);
				//Cuerpo del mensaje a enviar
				$mensaje='<html>
							<head>
								<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
								<title>Archivo Generado</title>
							</head>
			 				<body>
								<p>Estimado (a):</p>
								<b>Rodrigo</b><br>
								<i>Presente.</i><br><br>
								Junto con saludarle, se adjunta archivo para generar campaña: <b>'.$dat_file.'</b>  
								asociada a hoy <b>'.date('d-m-Y').'</b>. Por favor indicar al departamento de Infraestructura 
								que cree la misma en el Elastix por cada Ejecutiva de Cobranza.<br><br>

								Sin otro motivo en particular<br>
								Saluda atentamente,<br><br>
								<b>Administrador Sistema 2080sys Ltda.<br>Para campaña TCH</b> 
			 				</body>
			 			</html>';

				if(Biblioteca::envioCorreo($destinatarios, $path_adjuntos, $mensaje))
					$logger->debug("Correo enviado exitosamente, favor revisar");
				else
					$logger->debug("Hubo errores al enviar el correo. Pero el archivo fue generado. El archivo se ha de considerar como procesado. Revise ".$path_base_log_carga."campanas/".$fecha."/".$dat_file." ");
				//->Paso 3.5: Moviendo archivos a procesados
				$logger->debug("Moviendo archivos a procesados");
				exec('cd '.$path_base_log_carga.'proceso/; mv *.xlsx '.$path_base_log_carga.'procesados/; ');
				exec('cd '.$path_base_log_carga.'proceso/; mv *.txt '.$path_base_log_carga.'procesados/; ');
			} else {
				echo "Archivo csv no generado, no hay argumentos en el libro xlsx. Fijarse si, al iniciar el script, éste no se encuentra en la subcarpeta inicio junto con un archivo txt ";
				$logger->debug("Archivo csv no generado, no hay argumentos en el libro xlsx. Fijarse si, al iniciar el script, éste no se encuentra en la subcarpeta inicio junto con un archivo txt ");
				exec('cd '.$path_base_log_carga.'proceso/; mv *.xlsx '.$path_base_log_carga.'input/; ');
				exec('cd '.$path_base_log_carga.'proceso/; mv *.txt '.$path_base_log_carga.'input/; ');
			}


		}

<?php
	ini_set("memory_limit","16000M");
	date_default_timezone_set('America/Santiago');
	define("LOG_SI","1");

	//Biblioteca general, allá conectará el plugin de phpexcel y el de los correos
	require_once('../biblioteca.php');
	require_once('../conexion.php');
	//****************************************************************************
	//                   Conexion BD y carpetas de proceso
	//****************************************************************************
	$user 					= 'postgres';
	$passwd         		= 'secret';
	$db             		= 'bd';
	$host           		= '127.0.0.1'; 
	$port 					= 5432;
	$strCnx 				= "host=$host port=$port dbname=$db user=$user password=$passwd";
	$con 					= pg_connect ($strCnx) or die ("Error de conexion.". pg_last_error());

	//Si su proyecto no se encuentra en /var/www/html, considere corregir esto de las rutas absolutas
	$path_proyecto = "/var/www/html/archivos_y_cargas/";
	$path_base = "/var/www/html/archivos_y_cargas/";
	$path_base_log_carga	= $path_base."cargaDatosDavila/";

	$path 					= $path_base;
	$path_in 				= $path_base_log_carga."input/";
	$path_proceso 			= $path_base_log_carga."proceso/";
	$path_out				= $path_base_log_carga."out/";
	$path_logs				= $path_base_log_carga."logs/";
	$path_noprocesados      = $path_base_log_carga."no_procesado/";	
	$activo					= 1;
	$suf_in					= "_in";
	$suf_proceso			= "_pro";
	$suf_out				= "_out";
	$archivo_datos          = "";

	//Inicialización de dos variables clave para generar archivos xlsx con errores. Las rutas y los archivos
	//sólo serán materializados más abajo en el código, en el caso que existan errores que no pasen la validación.
	$fecha_carpeta_adjuntos=date('Ymd');
	$path_adjunto  = $path_proyecto."errores/"; 
	$path_adjuntos = $path_adjunto.$fecha_carpeta_adjuntos; //Repositorio donde se guardarán los archivos de errores, si existiesen



	//****************************************************************************
	//                   LOG4PHP
	//****************************************************************************
	define('LOG4PHP_DIR', $path_base.'utils/log4php-2.3.0/src/main/php'); 
	define('LOG4PHP_CONFIGURATION',$path_base_log_carga.'config.xml');  //Ambiente produccion log
	require_once(LOG4PHP_DIR.'/Logger.php'); 
	Logger::configure(LOG4PHP_CONFIGURATION);
	$logger = Logger::getLogger('loggerCarga');
	$logger->debug("Incio log4php");


	//****************************************************************************
	//         INSERT ERRORS
	//****************************************************************************
	function ins_error($tabla, $descripcion,$con){
		$sql_error=	"INSERT INTO $tabla 
					( descripcion, fecha) 
					VALUES 
					('$descripcion', '".date('Y-m-d H:i:s')."')
					";
		$rs_nuevo_error	= pg_query($con,$sql_error);
	}

	function validateDate($date){
    	if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$date)) {
   
	       return true;
	   }else{
	       return false;
	   }
	}

	//************************************************************************************************************************************************************
	//          Funciones útiles
	//************************************************************************************************************************************************************

	//Genera un array con los datos duplicados de uno, lo contrario a array_unique
	function array_duplicate($array){
		if(is_array($array)){
			$countedvals = array_count_values($array); 
			$filtered = array_filter($countedvals, function($var){ 
			return ($var > 1); 
			}); 
			$filtered = array_keys($filtered); 
			return $filtered;
		} else {
			return false;
		}
	}

	//************************************************************************************************************************************************************
	//          Función que genera un archivo xlsx con los registros malos
	//************************************************************************************************************************************************************
	function GeneraExcel($isapre=null){
  
		global $path_adjuntos;
		$fecha_hoyarchivo = date('dmY');		
		$dat_file = "-Errores_no cargados_".$isapre."_" . $fecha_hoyarchivo . ".xlsx"; //-->Nombre del Fichero
 		$nombre_archivo = $dat_file; 
		$objPHPExcel    = new PHPExcel(); 
		$objPHPExcel->getProperties()->setCreator("2080Sys")
								  	->setLastModifiedBy("2080Sys")
								  	->setTitle("Errores carga")
								  	->setSubject("Problemas Carga")
								  	->setDescription("Archivo con los errores de carga");                              
	  
		//-->titulo del archivo de retroalimenatcion
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', "ESTADO");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B1', "PREVISION");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C1', "ID INGRESO");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D1', "NOMBRE PACIENTE");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E1', "RUT PACIENTE");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F1', "TELÉFONOS");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G1', "TELÉFONOS");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H1', "TELÉFONO MOVIL 1");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I1', "TELÉFONO MOVIL 2");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('J1', "MOTIVO ADMISION");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('K1', "UNIDAD HOSP. DE INGRESO");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('L1', "FECHA ADMISIÓN");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('M1', "FECHA ALTA");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('N1', "RUT GIRADOR");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('O1', "NOMBRE GIRADOR");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('P1', "TELEFONO GIRADOR");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('Q1', "LIQUIDACION NRO.");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('R1', "PAM");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('S1', "DIAGNOSTICO DE EGRESO");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('T1', "FECHA CIERRE");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('U1', "FECHAENVÍOISAPRE");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('V1', "FECHA ENTREGA EJECUTIVO");  
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('W1', "FECHA ENTREGA PACIENTE/ISAPRE");    
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('X1', "TOTAL SALDO");            
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('Y1', "PREVISIÓN");            
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('Z1', "ID INGRESO");               
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AA1', "DIAS");      
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AB1', "CLASIFICADOR");          
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AC1', "RANGO"); 
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AD1', "ERROR");          
		
		
		$criterio_busqueda_isapre = $isapre=='ISAPRE CONSALUD' ? " and di.ing_prevision = 'ISAPRE CONSALUD' " : " and di.ing_prevision != 'ISAPRE CONSALUD' ";
	
		//--> Obteniendo las gestiones del día                 
		$sql_gestion ="
		select  
		di.ing_estado as \"ESTADO\", 
		di.ing_prevision as \"PREVISION\",
		di.id as \"ID INGRESO\", 
		dp.pac_nombre as \"NOMBRE PACIENTE\", 
		dp.rut as \"RUT PACIENTE\", 
		dp.pac_telefono1 as \"TELEFONOS1\",
		dp.pac_telefono2 as \"TELEFONOS2\",
		dp.pac_celular1 as \"TELEFONO MOVIL 1\",
		dp.pac_celular2 as \"TELEFONO MOVIL 2\",
		di.motivo_admision as \"MOTIVO ADMISION\", 
		ac.unidad_hosp_ingreso as \"UNIDAD HOSP. DE INGRESO\", 
		di.ing_fecha_admision as \"FECHA ADMISION\",
		di.ing_fecha_alta as \"FECHA ALTA\", 
		di.rut_girador as \"RUT GIRADOR\", 
		di.nombre_girador  as \"NOMBRE GIRADOR\", 
		di.fono_girador as \"TELEFONO GIRADOR\",
		dl.id as \"LIQUIDACION NRO.\", 
		ac.pam as \"PAM\",
		di.diagnostico_egreso as \"DIAGNOSTICO DE EGRESO\", 
		ac.fecha_cierre as \"FECHA CIERRE\",
		dl.fecha_envio_isapre as \"FECHAENVÍOISAPRE\",
		dl.fecha_entrega_ejecutivo as \"FECHA ENTREGA EJECUTIVO\",
		dl.fecha_entrega_isapre as \"FECHA ENTREGA PACIENTE/ISAPRE\",
		dl.liq_saldo as \"TOTAL SALDO\", 
		di.ing_prevision as \"PREVISION\",
		SPLIT_PART(di.id::TEXT,'-',1) as \"ID_INGRESO\", 
		dl.dias_deuda as \"DIAS\",
		ac.clasificador as \"CLASIFICADOR\",
		ac.identificador as \"RANGO\", 
		dl.mensaje_error as \"ERROR\"    
		
		from cargas c
	
		join dav_liquidacions_error dl on dl.carga_id=c.id
		left join dav_pacientes_error dp on dp.rut=dl.rut_id and dp.carga_id =c.id 
	 	left join dav_ingresos_error di on di.carga_id=c.id and dl.rut_id=di.rut_id  and di.id=dl.ingreso_id
		left join users u on dp.user_id=u.id
		left join dav_auxiliar_carga_error ac on ac.liquidacion_id=dl.id AND ac.rut_id = dl.rut_id and ac.carga_id=c.id and di.id=ac.ingreso_id  
		 
		where c.id in((select id from cargas where estado_davila=1)) 
		".$criterio_busqueda_isapre." 
		order by dl.id asc";         
	 
		$rs = pg_query($sql_gestion);
		$res_gestion = pg_fetch_all($rs);
			  
		if(count($res_gestion)>0 && (is_array($res_gestion) || is_object($res_gestion))) {
		   	//--> Con gestion asociada
		   	$num_linea=2;
		   	$i=1;
 
			foreach($res_gestion as $valor){
				//-->Contenido del Excel|
				//Un split a los id
				$id_ingreso = explode("-",strval($valor['ID_INGRESO']));
				//Y un formateo a los teléfonos
				$telefono1 = strval($valor['TELEFONOS1'])==0?"":strval($valor['TELEFONOS1']);
				$telefono2 = strval($valor['TELEFONOS2'])==0?"":strval($valor['TELEFONOS2']);
				$celular1  = strval($valor['TELEFONO MOVIL 1'])==0 ? "":strval($valor['TELEFONO MOVIL 1']);
				$celular2  = strval($valor['TELEFONO MOVIL 2'])==0 ? "":strval($valor['TELEFONO MOVIL 2']);
				$fono_girador = strval($valor['TELEFONO GIRADOR'])==0 ? "":strval($valor['TELEFONO GIRADOR']);
				//Formateo de fechas previas
				$valor['FECHA ADMISION']                 = ($valor['FECHA ADMISION']==0 || $valor['FECHA ADMISION']=='1899-12-30')                              ? ""  : date('d-m-Y',strtotime($valor['FECHA ADMISION']));
				$valor['FECHA ALTA']                     = ($valor['FECHA ALTA']==0 || $valor['FECHA ALTA']=='1899-12-30')                                      ? ""  : date('d-m-Y',strtotime($valor['FECHA ALTA']));
				$valor['FECHA CIERRE']                   = ($valor['FECHA CIERRE']==0 || $valor['FECHA CIERRE']=='1899-12-30')                                  ? ""  : date('d-m-Y',strtotime($valor['FECHA CIERRE']));
				$valor['FECHAENVÍOISAPRE']               = ($valor['FECHAENVÍOISAPRE']==0 || $valor['FECHAENVÍOISAPRE']=='1899-12-30')                          ? ""  : date('d-m-Y',strtotime($valor['FECHAENVÍOISAPRE']));
				$valor['FECHA ENTREGA EJECUTIVO']        = ($valor['FECHA ENTREGA EJECUTIVO']==0 || $valor['FECHA ENTREGA EJECUTIVO']=='1899-12-30')            ? ""  : date('d-m-Y',strtotime($valor['FECHA ENTREGA EJECUTIVO']));
				$valor['FECHA ENTREGA PACIENTE/ISAPRE']  = ($valor['FECHA ENTREGA PACIENTE/ISAPRE']==0 || $valor['FECHA ENTREGA PACIENTE/ISAPRE']=='1899-12-30')? ""  : date('d-m-Y',strtotime($valor['FECHA ENTREGA PACIENTE/ISAPRE']));
		
				//Completando los datos
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$num_linea, $valor['ESTADO']);                      
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$num_linea, $valor['PREVISION']);                      
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$num_linea, strval($valor['ID INGRESO']));                      
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$num_linea, $valor['NOMBRE PACIENTE']);                      
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.$num_linea, $valor['RUT PACIENTE']);                      
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$num_linea, $telefono1);                      
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$num_linea, $telefono2);                      
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$num_linea, $celular1);                      
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.$num_linea, $celular2);                      
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('J'.$num_linea, $valor['MOTIVO ADMISION']);                      
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('K'.$num_linea, $valor['UNIDAD HOSP. DE INGRESO']);                      
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('L'.$num_linea, $valor['FECHA ADMISION']);  //Ya formateado                    
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('M'.$num_linea, $valor['FECHA ALTA']);      //Ya formateado                
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('N'.$num_linea, $valor['RUT GIRADOR']);                      
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('O'.$num_linea, $valor['NOMBRE GIRADOR']);                      
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('P'.$num_linea, strval($fono_girador));                      
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('Q'.$num_linea, $valor['LIQUIDACION NRO.']);                      
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('R'.$num_linea, $valor['PAM']);                      
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('S'.$num_linea, $valor['DIAGNOSTICO DE EGRESO']);                      
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('T'.$num_linea, $valor['FECHA CIERRE']);                      
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('U'.$num_linea, $valor['FECHAENVÍOISAPRE']);                      
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('V'.$num_linea, $valor['FECHA ENTREGA EJECUTIVO']);                      
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('W'.$num_linea, $valor['FECHA ENTREGA PACIENTE/ISAPRE']);                      
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('X'.$num_linea, $valor['TOTAL SALDO']);                      
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('Y'.$num_linea, $valor['PREVISION']);                      
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('Z'.$num_linea, $id_ingreso[0]);                      
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AA'.$num_linea, $valor['DIAS']);                      
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AB'.$num_linea, $valor['CLASIFICADOR']);                      
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AC'.$num_linea, $valor['RANGO']);                                         
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AD'.$num_linea, $valor['ERROR']);                      
		
				$num_linea++;
				$i++;
			} //Fin foreach para la campaña
	 
			//--> Cerrando el excel
			$objPHPExcel->setActiveSheetIndex(0);        
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
			$objWriter->save($path_adjuntos.$nombre_archivo);
			return $nombre_archivo;
		} else {
			if($isapre=='')
				var_dump("Sin errores en el caso otras isapres por lo que no se crea archivo");
			else
				var_dump("Sin errores en el caso ".$isapre." por lo que no se crea archivo");
	 
		}
	}

	//************************************************************************************************************************************************************
	//          Funciones para insertar datos (pasando por parametro el string del dat)
	//************************************************************************************************************************************************************

	//Función que servirá para "desactivar" cargas anteriores. Esta función será ejecutada al inicio del script
	//Sólo se ejecutará en el caso que existan archivos nuevos por cargar a la BD
	function reset_estado_davila(){
		//-->Variables globales para tomar datos desde afuera de esta funcion
		global $con;
		//al resetear las otras cargas, si existiesen, se garantiza que la última carga de datos será la definitiva
		$sql_reset_cargas 	= "
							update cargas set estado_davila=0 where id in(
								(select id from cargas where estado_davila=1));
								";
			$sql_reset_cargas  = pg_query($con,$sql_reset_cargas);	
	}

	//Funcion que recibe el tipo de registro 1, para crear nueva carga (linea inicial del archivo)
	function insertarCarga($linea,$tipo_carga,$fecha_nueva_carga,$archivo_datos){
	 	//-->Variables globales para tomar datos desde afuera de esta funcion
	 	global $archivo_datos;
	 	global $con;
	 	global $fecha_nueva_carga;

	 	$tipo_registro 	= 1;
		$tipo_registro 	= trim($tipo_registro);

		$cod_agencia 	= "2080";
		$cod_agencia 	= trim($cod_agencia);

		$nombre_agencia = "CALL_2080";
		$nombre_agencia = trim($nombre_agencia);
		//Reseteando las cargas anteriores
		reset_estado_davila();

		//Creando nueva carga y obtener id
		$sql_nueva_carga=	"INSERT INTO cargas 
							(car_fechaini, car_file, car_estado, tipo_registro, nombre_agencia, cod_agencia, tipocarga_id) 
							VALUES 
							('".$fecha_nueva_carga."','$archivo_datos','EN CURSO','$tipo_registro','$nombre_agencia', '$cod_agencia', $tipo_carga)	
							RETURNING id
							";
		
		$rs_nuevacarga	= pg_query($con,$sql_nueva_carga);
		$row_nuevacarga	= pg_fetch_row($rs_nuevacarga); 
		$id_nueva_carga	= $row_nuevacarga['0'];

		echo "id nueva carga".$id_nueva_carga;
		return $id_nueva_carga;
	}

	function eliminarCarga($id_carga){
		global $con;
		//Creando nueva carga y obtener id
		$sql_elimina_carga=	"DELETE FROM cargas WHERE id=".$id_carga."";
		$rs_eliminacarga	= pg_query($con,$sql_elimina_carga);
   	}

	//Función que validará los números de teléfono

	function validaTelefono($numero_fono)
	{
		//Previo es una validación por si el número tiene 11 caracteres por tener el código 56: 
		if(strlen($numero_fono)==11 && substr($numero_fono,0,2)=="56"){  //56982004330 con código país 5622215101
			$fono = $numero_fono;
			if(substr($numero_fono,0,3)=="569")
				$numero_fono="0".substr($fono,2);
			else
			$numero_fono=substr($fono,2);
		}
		if(strlen($numero_fono)==8 && in_array(intval(substr($numero_fono,0,1)),range(5,9))){
			$numero_fono = "09".$numero_fono;   
		} elseif(strlen($numero_fono)==8 && (substr($numero_fono,0,1)=="2" )) {
			$numero_fono="2".$numero_fono;
		} //Con regiones no se puede validar de esta manera pues la diversidad de códigos imposibilita este pr-formateo
		//Validando
		//Cantidad de caracteres no son ni 9 ni 10 luego del formateo anterior
		if(strlen($numero_fono)<9 || strlen($numero_fono)>10) {
			return "0";
		//Cantidad de caratceres luego del formateo anterior es de 10, debe ser solo por el factor 09-XXXXXXXX
		} elseif(strlen($numero_fono)==10) { 				
			if( intval(substr($numero_fono,0,1)) != 0 				|| 		//1=> cero
				intval(substr($numero_fono,1,1)) != 9 				|| 		//2=> nueve
				!in_array(intval(substr($numero_fono,2,1)),range(5,9)) ){	//3=> 5, 6, 7, 8 o 9
				//El resto de caracteres pueden ser cualquier número...
					return "0";
				} else {
					return $numero_fono;
				}
		//Cantidad de caracteres luego del formateo anterior es de 9, o es de red fija o es de un celular sin cero
		} elseif(strlen($numero_fono)==9 && substr($numero_fono,0,1) != "9") { //Red fija 222215101 no un celular sin el 9 o 09...
			$codigo_num =substr($numero_fono,0,2);
				//A revisar reglas!, para santiago código es "22"
				if(substr($numero_fono,0,2) == "22") //Ya se cumple que las primeras 2 cifras sean 2 y 2 respectivamente, para RM
				{
					if( !in_array(intval(substr($numero_fono,2,1)),range(1,9)) ){	//3=> 1,2,3,4,5,6,7,8 o 9
						return "0";
					} else {
						return $numero_fono;
					}
				} else  { //Cualquier otro digito se asume como regional
					if( !in_array(intval(substr($numero_fono,0,1)), range(3,7)) || //1=>3,4,5,6 o 7
						!in_array(intval(substr($numero_fono,1,1)), range(1,8)) || //2=>1,2,3,4,5,6,7, o el 8 
						!in_array(intval(substr($numero_fono,2,1)), range(1,9)) ){  //3=>1 al 9 no cero
						return "0";
					} else {
						return $numero_fono;
					}
				}

		} elseif(strlen($numero_fono)==9 && substr($numero_fono,0,1) == "9") {
			$numero_fono="0".$numero_fono;
			if( intval(substr($numero_fono,0,1)) != 0 				|| 		//1=> cero
					intval(substr($numero_fono,1,1)) != 9 				|| 		//2=> nueve
					!in_array(intval(substr($numero_fono,2,1)),range(5,9)) ){	//3=> 5, 6, 7, 8 o 9
					//El resto de caracteres pueden ser cualquier número...
						return "0";
					} else {
						return $numero_fono;
					}
				
		} else {
			return "0";
		}
	}	  

	//Para la inserción de teléfono en el elastix
	function insertarTelefonia($rut_id,$ingreso_id,$carga_id,$numero_fono,$prioridad,$cod_fono,$created,$modified,$con){
		//Se pasa directamente, puede ser 1 (red fija) y 2 (celular) en $cod_fono
		$cod_area			= NULL;
		$telefonia_activo	= 1;

		//Formateo de número de teléfono

		/* Paso A: Limpieza de formato */
		$numero_fono = trim($numero_fono);
		$numero_fono = str_replace("{", "", $numero_fono);
		$numero_fono = str_replace("+", "", $numero_fono);
		$numero_fono = str_replace("}", "", $numero_fono);
		$numero_fono = str_replace("-", "", $numero_fono);
		$numero_fono = str_replace(" ", "", $numero_fono);
		
		/* Paso B: reglas de negocio a verificar */
		if(!isset($numero_fono)) //Si el número no existe no se insertará en las tablas
		{
			$numero_fono = null;
			$fono_error = 1;
			$insertar_fono = false;
		}	
		elseif(validaTelefono($numero_fono)=="0") //Fono no válido
		{
			$fono_error = 1;
			$insertar_fono = true;
		}
		else
		{
			$numero_fono = validaTelefono($numero_fono); //Fono válido pero formateado para elastix
			$fono_error = 0;
			$insertar_fono = true;
		}
		
		//Se debe evaluar que el número de teléfono respete las normas de formateo
		
		$origen				= 1; //--> Viene en la carga de DAVILA
		$viene_hoy			= 1;
		
		$sql_telefonia_exists = "select exists(select rut_id, ingreso_id, numero_fono, cod_fono 
								from dav_telefonia 
								where 
								rut_id 			= '".$rut_id."' 
								and cod_fono 	= ".$cod_fono." 
								and numero_fono = '".$numero_fono."' 
								and ingreso_id  = '".$ingreso_id."' 
								) as resultado 
								";

		$rs_telefonia_exists  	= pg_query($con,$sql_telefonia_exists);
		$Result 	  	  		= pg_fetch_assoc($rs_telefonia_exists);
		$tabla 					= "";

		if($Result['resultado']=="t"){
			//-->Existe en la tabla
			$tabla 				= "dav_telefonia_solucion_mod"; 
		} else {
			//--> No existe en la tabla
			$tabla 				= "dav_telefonia_solucion_ins"; 
		}	
		//--> Chequeando en solucion
		$sql_telefonia_exists_sol = "select exists(select rut_id, ingreso_id, numero_fono, cod_fono 
								from dav_telefonia_solucion_ins
								where 
								rut_id 			= '".$rut_id."' 
								and cod_fono 	= ".$cod_fono." 
								and numero_fono = '".$numero_fono."' 
								and ingreso_id   = '".$ingreso_id."' 
								) as resultado 
								";

		$rs_telefonia_exists_sol = pg_query($con,$sql_telefonia_exists_sol);
		$Result_sol	  	  		= pg_fetch_assoc($rs_telefonia_exists_sol);
		$tabla_sol				= "";
		
		if($Result_sol['resultado']=="t"){
			//-->Existe en la tabla que viene en la misma carga, por lo que no se cargará 2 veces							
		} elseif($insertar_fono) {
			//--> No existe en la tabla, pero ojo, sólo se insertará si el número respeta las normas
			$sql_ins_telefonia 	= "insert into ".$tabla." 
								(rut_id,ingreso_id,carga_id,created,modified,cod_fono,numero_fono,telefonia_activo,fono_error,origen,viene_hoy,prioridad) 
								values 
								('$rut_id','$ingreso_id',$carga_id,'$created','$modified',$cod_fono,'$numero_fono',$telefonia_activo,$fono_error,$origen,$viene_hoy,$prioridad)
								";

			$rs_ins_telefonia  = pg_query($con,$sql_ins_telefonia);	
		}

	}
	// Función que busca formatear una fecha al formato de almacenado de la BD obtenida del modo dd-mm-yyyy
	function formateaFecha($fecha) {
		try{
			$unix_date 					= ($fecha - 25569) * 86400;
			$excel_date 				= 25569 + ($unix_date / 86400);
			$unix_date 					= ($excel_date - 25569) * 86400;
			$date_ 						= gmdate("Y-m-d", $unix_date);
			return $date_;
		}
		catch(Exception $e){
			return false;
		}
		
	}
	//Función que formatea una fecha al formato de almacenado de la BD obtenida como texto, formato dd/mm/yyyy hh:mm. Sólo almacenará Y-m-d
	function formateaFechaLarga($fecha) {
		try{

			$date_formateada = trim($fecha);
			$df = explode(" ",$date_formateada);
			$dfor = $df[0];
			$dfor = str_replace("/","-",$dfor);
			
			return date('Y-m-d',strtotime($dfor));
		}
		catch(Exception $e){
			return false;
		}
	}


	//Formateador de rut
	//La función siempre devolverá "-" al encontrarse con sólo strings. Esta función no valida, sólo formatea
	function formatea_rut($rut)
	{
		$rut_formateado = trim($rut); //Eliminando espacios
		$rut_formateado = str_replace('.','',$rut_formateado); //Eliminando puntitos que pudiese tener
		$rut_formateado = str_replace('-','',$rut_formateado); //Eliminando guiones
		$rut_formateado = preg_replace('/[a-zA-ZáéíóúÁÉÍÓÚ]/i','',$rut_formateado); //Eliminando letras u otros
		//Ahora a trabajarlo más. A colocarlo de la forma XXXXXXX-X
		$dv = substr($rut_formateado,-1,1);
		$cuerpo = substr($rut_formateado,0,-1);
		$rut_formateado = $cuerpo."-".$dv;
		return $rut_formateado;

	}
	//Validador de rut
	function valida_rut($rut)
	{
		formatea_rut($rut);
		if (!preg_match("/^[0-9.]+[-]?+[0-9kK]{1}/", $rut)) {
			return false;
		}

		$rut = preg_replace('/[\.\-]/i', '', $rut);
		$dv = substr($rut, -1);
		$numero = substr($rut, 0, strlen($rut) - 1);
		$i = 2;
		$suma = 0;
		foreach (array_reverse(str_split($numero)) as $v) {
			if ($i == 8)
				$i = 2;
			$suma += $v * $i;
			++$i;
		}
		$dvr = 11 - ($suma % 11);

		if ($dvr == 11)
			$dvr = 0;
		if ($dvr == 10)
			$dvr = 'K';
		if ($dvr == strtoupper($dv))
			return true;
		else
			return false;
	}


	//**********************************************************************************************************************
	// 												EJECUCION PROCESO
	//**********************************************************************************************************************
	//****************************************************************************************************
	//                   Chequear si existe archivo de carga antes de ejecutar todo el proceso
	//****************************************************************************************************
	$directorio	 		= opendir($path_in);
	$arr_archivo_csv 	= array();
	$file_mdb 			= "";
	$sw_ok 				= 0;

	$logger->debug("Se ejecuta Crontab de carga de Datos de Clinica davila");
	$logger->debug("Chequeando si en la carpeta INPUT existe algun archivo de carga para procesar");
	while ($archivo = readdir($directorio)){
		if($archivo!='.' and $archivo!='..'){ 
			$name_archivo_original=$archivo;
			$arr_archivo=explode(".", $name_archivo_original);
			if($arr_archivo[1]=="xlsx" || $arr_archivo[1]=="xls"){
				$logger->debug("Nombre Original del archivo a procesar:".$name_archivo_original);
				$file_mdb=$name_archivo_original;
				$sw_ok=1;
				break;
			} else {
				$sw_ok=0;
			}
		}	
	}
	$logger->debug($sw_ok);
	$nombre_archivo_excel_completo = $file_mdb;

	
	//****************************************************************************************************
	//                   Como existe el archivo se debe mover a la carpeta proceso
	//****************************************************************************************************
	$sw_ok_proceso=0;
	if($sw_ok==1){
		$logger->debug("Moviendo el Archivo a la carpeta de INPUT a PROCESO");
		$sw_ok_proceso = 1;	
		$directorio_proceso = opendir($path_proceso);
		//--> Moviendo el archivo de la carpeta INPUT a PROCESO
		try{
			exec('cd '.$path_base.'cargaDatosDavila/input/; mv *.xlsx '.$path_base.'cargaDatosDavila/proceso/; ');
		}
		catch(Exception $e){
			$logger->debug("Problemas, no se mueve archivo a procesado");
			

		}
	} else {
		$sw_ok_proceso = 0;
		$logger->debug("No existe Archivo a procesar");
	}

	$sw_ok_out=0;
	$fecha_volcado="";
	if($sw_ok_proceso==1){
		$logger->debug("Comienzo de la carga de datos como tal");
		//--> Buscar ultimo ID de la carga
		$sql_carga_antigua = "select id, date(car_fechaini) from cargas where car_estado='Finalizado' order by id desc limit 1 ";
		$rs_carga_antigua  = pg_query($con,$sql_carga_antigua);

		if(count($rs_carga_antigua)>0){
			$fil 	= pg_numrows($rs_carga_antigua); 
			if($fil >0){
				for($cont=0;$cont<$fil;$cont++) { 
			    	$id 			= pg_result($rs_carga_antigua,$cont,0);
			    	$fecha_volcado	= pg_result($rs_carga_antigua,$cont,1);
			    	$cont 			= $fil; 			
			    }
			} else {
				$logger->debug("No existe ninguna carga aun");
			}    
		} else {
			$logger->debug("No existe ninguna carga aun");
		}


		if($fecha_volcado!=date('Y-m-d')){
			$sw_ok_out = 1;	
			$logger->debug("Comienzo de la carga de datos como tal pero en Proceso");
			$fecha_hoy 		= date("Ymd");
			$fecha 			= date("Ymd");
			$directorio	 	= opendir($path_proceso);
			$arr_archivo_csv= array();
			$file_mdb 		= "";
			$sw_ok 			= 0;
			$logger->debug("Archivos listado leido: $archivo");
			while ($archivo = readdir($directorio)){
				if($archivo!='.' and $archivo!='..'){ 
					$name_archivo_original=$archivo;
					$arr_archivo=explode(".", $name_archivo_original);

					if($arr_archivo[1]==="xlsx" || $arr_archivo[1]==="xls"){
						//echo $arr_archivo;
						$file_mdb=$name_archivo_original;
						$archivo_datos = $file_mdb;
						$sw_ok=1;
					} else {
						//echo "no debería pasar por acá";
						$sw_ok=0;
					}
				}	
			}
			$logger->debug($sw_ok);
			$logger->debug($path_proceso); //Muestra efectivamente la ruta de proceso
			
			//--> Volcado de los registros de la BD Actual
			if($sw_ok==1){
				//--> Paso 0: Creando carga
					$logger->debug("Creando registro en tabla cargas");
					$linea 			= "";
					$tipo_carga     = 1;
					$fecha_nueva_carga = date('Y-m-d h:i:s');
					$id_nueva_carga = insertarCarga($linea,$tipo_carga,$fecha_nueva_carga,$archivo_datos);
					$logger->debug("ID nueva carga:".$id_nueva_carga);
					//Ahora que la carga fue creada, se mueve el archivo txt
					exec('cd '.$path_base.'cargaDatosDavila/input/; mv *.txt '.$path_base.'cargaDatosDavila/proceso/; ');

					//--> Paso 1: Volcado de la BD actual a Historico
					//--> No hay volcado porque se hace al final del día de los pacientes, ingresos y liquidaciones que estuvieron activas.

					//--> Paso 2: Borrar informacion previas de las tablas de PASO SOLUCIONES
					$logger->debug("Borrando las tablas SOLUCION");
					
					//--> 	Paso 1.1: SOLUCIONES PACIENTE
					$sql_pacientes_ins		= "delete from dav_pacientes_solucion_ins";
					$sql_pacientes_mod		= "delete from dav_pacientes_solucion_mod";
					pg_query($con,$sql_pacientes_ins);
					pg_query($con,$sql_pacientes_mod);

					//--> 	Paso 1.2: SOLUCIONES INGRESOS
					$sql_ingresos_ins		= "delete from dav_ingresos_solucion_ins";
					$sql_ingresos_mod		= "delete from dav_ingresos_solucion_mod";
					pg_query($con,$sql_ingresos_ins);
					pg_query($con,$sql_ingresos_mod);

					//--> 	Paso 1.3: SOLUCIONES LIQUIDACIONES
					$sql_liquidacions_ins	= "delete from dav_liquidacions_solucion_ins";
					$sql_liquidacions_mod	= "delete from dav_liquidacions_solucion_mod";
					pg_query($con,$sql_liquidacions_ins);
					pg_query($con,$sql_liquidacions_mod);
					
					//-->	Paso 1.4: SOLUCIONES TELEFONIA
					$sql_telefonia_ins		= "delete from dav_telefonia_solucion_ins";
					$sql_telefonia_mod		= "delete from dav_telefonia_solucion_mod";
					pg_query($con,$sql_telefonia_ins);
					pg_query($con,$sql_telefonia_mod);

					$logger->debug("Se borraron las tablas soluciones temporales");

					//--> Paso 3: Rescatando el TIPO DE CARGA DE DATOS
					sleep(3);

					if(isset($argv[1])){
						if(is_numeric($argv[1])){
							$tipo_carga = $argv[1];
						}else{
							$tipo_carga = 1;
						}
					}else{
						$tipo_carga = 1;
					}

					//--> Paso 4: Leyendo las lineas del archivo
					///--> Paso 4.1 Obteniendo las lineas del archivo	
					$file_dat = $path_proceso.$file_mdb;
					
					//--> Paso 4.2 verificando el leer archivo
					$archivo 		= $file_dat;
					$inputFileType 	= PHPExcel_IOFactory::identify($archivo);
					$objReader 		= PHPExcel_IOFactory::createReader($inputFileType);
					$objPHPExcel 	= $objReader->load($archivo);
					$sheet 			= $objPHPExcel->getSheet(0); 
					$highestRow 	= $sheet->getHighestRow(); 
					$highestColumn 	= $sheet->getHighestColumn();

					//Validación 0: cantidad de columas, se trabajará más adelante
					if($highestColumn != 'AE'){ //LEE LOS ENCABEZADOS DE COLUMNA. DEBEN SER IGUAL A LA COLUMNA AE
						$logger->debug("--- ERROR CRITICO, ARCHIVO XLSX DE CARGA NO POSEE LA MISMA CANTIDAD DE COLUMNAS QUE LO ESTABLECIDO, reprueba validación y carga de archivo"); 
						//Intención de suspender el proceso completo, se mueve el archivo a procesados
						exec('cd '.$path_base.'cargaDatosDavila/proceso/; mv *.xlsx '.$path_base.'cargaDatosDavila/procesados/');
						exec('cd '.$path_base.'cargaDatosDavila/proceso/; mv *.txt '.$path_base.'cargaDatosDavila/procesados/');
						//Luego, se devuelve el archivo. Se envía correo electrónico
						try
						{
							$cuerpo='<html>
								<head><meta http-equiv="Content-Type" content="text/html; charset=windows-1252"><title>Carga de datos</title></head>
								<body>
									<b>Estimados:</b><br>
									Buenos d&iacute;as. Comunico que NO se ha ejecutado el proceso de carga de datos con la informaci&oacute;n depositada por D&aacute;vila hoy <b>'.date('d-m-Y').'</b><br><br>
									El motivo es que el presente adjunto no contiene el formato establecido al poseer un
									numero de columnas distintos al especificado<br>
									Quedo atento a comentarios<br>
									Saludos cordiales<br>
									<b>Administrador Sistema COBRA 2080sys<br>para Clinica Davila.</b>	
								</body>
							</html>';
							$mail = new PHPMailer();			
							$mail->isSMTP();            
							$mail->Host = "ssl://mail.2080.cl";
							$mail->Port = 465; 
							$mail->SMTPAuth = true;                           
							$mail->Username =  "cobra@2080.cl";                            
							$mail->Password = "N25VLHx.zM";                                                
							$mail->SMTPSecure = "SSl";                                                 			
							$mail->From = "cobra@2080.cl";
							$mail->FromName = "Adm.Cobra 2080 - Davila";
							$mail->addAddress("carlos.orrego@staff.2080.cl");
							$mail->addAddress("carmen.aguirre@2080.cl");
							$mail->addAddress("rodrigo.correa@2080.cl");
							$mail->addAddress("arturo.guillen@2080.cl"); 			 			
							$mail->isHTML(true);			
							$mail->AddAttachment($path_base.'cargaDatosDavila/procesados/'.$nombre_archivo_excel_completo);
							
							$mail->Subject = "Clinica Davila: Carga de datos asociada al ".date('d-m-Y')." Finalizado.";				
							$mail->Body = $cuerpo;
				
							if(!$mail->send()) {
								echo "Mailer Error: " . $mail->ErrorInfo;
							}else {								
								echo '<p>Archivo de carga devuelto a remitentes. Gracias. Gracias!</p>';		
							}
						} 
						catch (phpmailerException $e) 
						{
							//echo $e->errorMessage(); //error messages from PHPMailer
							$logger->debug('Error Email: '.$e->errorMessage());
						} 
						catch (Exception $e) 
						{
							//echo $e->getMessage();
							$logger->debug('Error Email2: '.$e->getMessage());
						}
   
						$logger->debug("Finalizado envio de email devolviendo archivo.");

						//El proceso finalizó, nada más que cargar, por lo que se suspende la operación
						//Sólo resta eliminar la carga
						eliminarCarga($id_nueva_carga); 
						exit("Proceso finalizado por errores en archivo de carga");
					
					}
					//Si el if anterior no se manifiesta, entonces el script seguirá su curso normal
					$query_ce	= "select id from users where usu_activo=1 and agente_id is not null and perfil_id=62 order by agente_id";
					$rs_ce  	= pg_query($con,$query_ce);
					//Para setear 1 a 1 por paciente, las operadoras
					$coleccion_ce = array();
					$index_ce = 0; //Muy importante, para indexar el arreglo de la colección de las CE

					while($fila = pg_fetch_object($rs_ce)){
						array_push($coleccion_ce,$fila->id);
					}

					//Contadores varios
					$cuentaregistros_correctos = 0; //Registros correctos
					$cuentaregistros_error = 0; //Registros errados
					$registro_correcto = array(); //Almacena el número de fila correcto, menos uno
					$registro_error = array(); //Almacena el número de fila incorrecto, menos uno
					//Conformación de un vector con las liquidaciones, todas
					$coleccion_liquidacions = array();

					for ($row = 2; $row <= $highestRow; $row++){  
						array_push($coleccion_liquidacions, utf8_encode(trim($sheet->getCell("S".$row)->getValue())));
					}
					//Colección de liquidaciones duplicadas, no se repetirán acá pero sí se almacenan todas las repetidas acá
					$coleccion_liq_duplicados = array_duplicate($coleccion_liquidacions);
					$coleccion_liq_unicas = array_unique($coleccion_liquidacions);

					$arreglo_isapre = array('ISAPRE CONSALUD','');

					for ($row = 2; $row <= $highestRow; $row++){  
						//--> Paso 4.3 Obteniendo los datos por columnas
						$logger->debug("Procesando line:".$row);					

						$arreglo_lectura=array(
							'A' =>	utf8_encode(trim($sheet->getCell("A".$row)->getValue())),	//ESTADO
							'B' =>	utf8_encode(trim($sheet->getCell("B".$row)->getValue())),	//PREVISION
							'C' =>	utf8_encode(trim($sheet->getCell("C".$row)->getValue())),	//ID INGRESO
							'D' =>	utf8_encode(trim($sheet->getCell("D".$row)->getValue())),	//NOMBRES DEL PACIENTE
							'E' =>	utf8_encode(trim($sheet->getCell("E".$row)->getValue())),	//AP PATERNO PACIENTE
							'F' =>	utf8_encode(trim($sheet->getCell("F".$row)->getValue())),	//AP MATERNO PACIENTE
							'G' =>	utf8_encode(trim($sheet->getCell("G".$row)->getValue())),	//RUT PACIENTE
							'H' =>	utf8_encode(trim($sheet->getCell("H".$row)->getValue())),	//TELEFONOS - RED FIJA 1
							'I' =>	utf8_encode(trim($sheet->getCell("I".$row)->getValue())),	//TELEFONOS - RED FIJA 2
							'J' =>	utf8_encode(trim($sheet->getCell("J".$row)->getValue())),	//TELEFONO MOVIL- CELULAR 1
							'K' =>	utf8_encode(trim($sheet->getCell("K".$row)->getValue())),	//TELEFONO MOVIL-CELULAR 2
							'L' =>	utf8_encode(trim($sheet->getCell("L".$row)->getValue())),	//MOTIVO ADMISION
							'M' =>	utf8_encode(trim($sheet->getCell("M".$row)->getValue())),	//UNIDAD HOSPITAL INGRESO
							'N' =>	utf8_encode(trim($sheet->getCell("N".$row)->getValue())),	//FECHA ADMISION
							'O' =>	utf8_encode(trim($sheet->getCell("O".$row)->getValue())),	//FECHA ALTA
							'P' =>	utf8_encode(trim($sheet->getCell("P".$row)->getValue())),	//RUT GIRADOR
							'Q' =>	utf8_encode(trim($sheet->getCell("Q".$row)->getValue())),	//NOMBRE GIRADOR
							'R' =>	utf8_encode(trim($sheet->getCell("R".$row)->getValue())),	//FONO GIRADOR
							'S' =>	utf8_encode(trim($sheet->getCell("S".$row)->getValue())),	//NUMERO DE LIQUIDACION
							'T' =>	utf8_encode(trim($sheet->getCell("T".$row)->getValue())),	//PAM
							'U' =>	utf8_encode(trim($sheet->getCell("U".$row)->getValue())),	//DIAGNOSTICO DE EGRESO
							'V' =>	utf8_encode(trim($sheet->getCell("V".$row)->getValue())),	//FECHA DE CIERRE
							'W' =>	utf8_encode(trim($sheet->getCell("W".$row)->getValue())),	//FECHAENVIOISAPRE
							'X' =>	utf8_encode(trim($sheet->getCell("X".$row)->getValue())),	//FECHA ENTREGA EJECUTIVO
							'Y' =>	utf8_encode(trim($sheet->getCell("Y".$row)->getValue())),	//FECHA ENTREGA PACIENTE-ISAPRE
							'Z' =>	utf8_encode(trim($sheet->getCell("Z".$row)->getValue())),	//TOTAL SALDO
							'AA' =>	utf8_encode(trim($sheet->getCell("AA".$row)->getValue())),	//PREVISION
							'AB' =>	utf8_encode(trim($sheet->getCell("AB".$row)->getValue())),	//ID DE INGRESO
							'AC' =>	utf8_encode(trim($sheet->getCell("AC".$row)->getValue())),	//DIAS -importante
							'AD' =>	utf8_encode(trim($sheet->getCell("AD".$row)->getValue())),	//CLASIFICADOR
							'AE' =>	utf8_encode(trim($sheet->getCell("AE".$row)->getValue()))	//RANGO ex ID, EN BD ES LA COLUMNA "Identificador"
							
						);	

						//Reordenando valores en variables y formateando algunos de ellos si corresponde (teléfonos)
						//$rut_id 				= formatea_rut($arreglo_lectura["G"]); //Descomente para validar los rut
						$rut_id 				= $arreglo_lectura["G"];
						$ingreso_id 			= $arreglo_lectura["C"];  //LA COLUMNA AA SIRVE, PERO NO GUARDARÁ EL IDENTIFICADOR.
						$liquidacion_id 		= $arreglo_lectura["S"];
						
						$created 				= date('Y-m-d h:i:s');
						$modified				= $created;
						$carga_id				= $id_nueva_carga;

						//Nuevos
						$unidad_hosp_ingreso 	= $arreglo_lectura['M'];
						$pam 					= $arreglo_lectura['T'];
						$fecha_cierre 			= $arreglo_lectura['V'];
						//Formateo previo a fecha de cierre
						$fecha_cierre 			= (formateaFechaLarga($fecha_cierre)==0 || formateaFechaLarga($fecha_cierre)=='1899-12-30') ? "" : formateaFechaLarga($fecha_cierre);
						$clasificador 			= $arreglo_lectura['AD'];
						$rango 					= $arreglo_lectura['AE']; //Es el rango, en columna identificador
						//Fin nuevos


						$pac_nombre				= $arreglo_lectura["D"]." ".$arreglo_lectura["E"]." ".$arreglo_lectura["F"];
						//Corrigiendo los teléfonos, conviene formatearlo
						$pac_telefono1			= strlen($arreglo_lectura["H"])<=8 ? "" : $arreglo_lectura["H"];
						$pac_telefono2			= strlen($arreglo_lectura["I"])<=8 ? "" : $arreglo_lectura["I"];
						$pac_celular1			= strlen($arreglo_lectura["J"])<=8 ? "" : $arreglo_lectura["J"];
						$pac_celular2			= strlen($arreglo_lectura["K"])<=8 ? "" : $arreglo_lectura["K"];
						$pac_activo				= 1;


						$ing_activo 			= 1;
						$ing_prevision 			= $arreglo_lectura["B"];
						$ing_fecha_admision 	= $arreglo_lectura["N"];
						$ing_fecha_alta 		= $arreglo_lectura["O"];						
						$nombre_girador     	= $arreglo_lectura["Q"];
						if($nombre_girador != null || $nombre_girador != "" || $nombre_girador != "-")
						{
							//$rut_girador 		= formatea_rut($arreglo_lectura["P"]); //Descomentar para validar rut
							$rut_girador 		= $arreglo_lectura["P"];
							$fono_girador 		= $arreglo_lectura["R"];
						}
						else
						{
							$rut_girador 		= "";
							$fono_girador 		= "";
						}

						$ing_estado         	= $arreglo_lectura["A"];
						$arreglo_lectura['N'] 	= (formateaFechaLarga($arreglo_lectura['N'])==0 || formateaFechaLarga($arreglo_lectura['N'])=='1899-12-30') ? "" : formateaFechaLarga($arreglo_lectura['N']);
						$arreglo_lectura['O'] 	= (formateaFechaLarga($arreglo_lectura['O'])==0 || formateaFechaLarga($arreglo_lectura['O'])=='1899-12-30') ? "" : formateaFechaLarga($arreglo_lectura['O']);

						$ing_fecha_admision 	= $arreglo_lectura["N"];	
						$ing_fecha_alta 		= $arreglo_lectura["O"];	

						//Faltan 3 atributos nuevos
						$diagnostico_egreso 	= $arreglo_lectura["U"];
						$motivo_admision 		= $arreglo_lectura["L"];
						$saldo_total 			= 0;
						$ing_estado_alta 		= "ALTA";
									
						//--> Validaciones extras
						
						if(strlen($ing_prevision)>0)		$ing_prevision		="'".$ing_prevision."'"; 		else $ing_prevision 		='NULL';						
						if(strlen($rut_girador)>0)			$rut_girador 		="'".$rut_girador."'"; 			else $rut_girador 			='NULL';
						if(strlen($fono_girador)>0) 		$fono_girador 		="'".$fono_girador."'"; 		else $fono_girador			='NULL';						
						if(strlen($nombre_girador)>0)   	$nombre_girador 	="'".$nombre_girador."'"; 		else $nombre_girador 		='NULL';
						if(strlen($ing_fecha_admision)>0)   $ing_fecha_admision ="'".$ing_fecha_admision."'"; 	else $ing_fecha_admision 	='NULL';
						if(strlen($ing_fecha_alta)>0)   	$ing_fecha_alta 	="'".$ing_fecha_alta."'"; 		else $ing_fecha_alta 		='NULL';
						if(strlen($ing_estado)>0)   		$ing_estado 		="'".$ing_estado."'"; 			else $ing_estado 			='NULL';
						//Se debe agregar ahora, tres atributos adicionales producto a inserciones a la Base de datos
						if(strlen($diagnostico_egreso)>0)   $diagnostico_egreso ="'".$diagnostico_egreso."'"; 	else $diagnostico_egreso 	='NULL';
						if(strlen($motivo_admision)>0)   	$motivo_admision 	="'".$motivo_admision."'"; 		else $motivo_admision 		='NULL';
						if(strlen($saldo_total)>0)   		$saldo_total 		="'".$saldo_total."'"; 			else $saldo_total 			='NULL';
					   
					   	$liq_activo						= 1;
						$liq_garantia					= 'NULL';
						$liq_tiporiesgo					= 'NULL';	
						$liq_copagoclinica				= 0;
						$liq_copagohhmm					= 0;
						$liq_saldo						= $arreglo_lectura["Z"];
						$liq_saldogirador				= 0;
						//LO QUE REALMENTE IMPORTA ACÁ SON EL ID DE LA LIQUIDACIÓN, MONTO Y AL ID DE INGRESO ASOCIADO, PARA EFECTOS DE SER INFORMATIVO
						$dias_deuda						=$arreglo_lectura["AC"];

						//También importará mucho las distintas fechas, no olvidar el script y los INSERT
						$arreglo_lectura['W'] 			= (formateaFecha($arreglo_lectura['W'])==0 || formateaFecha($arreglo_lectura['W'])=='1899-12-30') ? "" : formateaFecha($arreglo_lectura['W']);
						$arreglo_lectura['X'] 			= (formateaFechaLarga($arreglo_lectura['X'])==0 || formateaFechaLarga($arreglo_lectura['X'])=='1899-12-30') ? "" : formateaFechaLarga($arreglo_lectura['W']);
						$arreglo_lectura['Y'] 			= (formateaFechaLarga($arreglo_lectura['Y'])==0 || formateaFechaLarga($arreglo_lectura['Y'])=='1899-12-30') ? "" : formateaFechaLarga($arreglo_lectura['W']);
						$fecha_envio_isapre 			= $arreglo_lectura["W"];	
						$fecha_entrega_ejecutivo 		= $arreglo_lectura["X"];	
						$fecha_entrega_isapre 			= $arreglo_lectura["Y"];		
						
						//-->validando datos
						if(strlen($fecha_envio_isapre)>0) 		$fecha_envio_isapre	="'".$fecha_envio_isapre."'"; 				else $fecha_envio_isapre =NULL;
						if(strlen($fecha_entrega_ejecutivo)>0) 	$fecha_entrega_ejecutivo	="'".$fecha_entrega_ejecutivo."'"; 	else $fecha_entrega_ejecutivo =NULL;
						if(strlen($fecha_entrega_isapre)>0) 	$fecha_entrega_isapre	="'".$fecha_entrega_isapre."'"; 		else $fecha_entrega_isapre =NULL;
						
						/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
						// Efectuando las validaciones, antes de proceder a insertar aspecto alguno //
						++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
						$mensaje_error = ''; //Inicialización del flag de error

						//->Validación 1: Liquidaciones duplicadas
						if(in_array($liquidacion_id,$coleccion_liq_duplicados)){
							$logger->debug("--- Error registro duplicado, reprueba validación:".$row); 
							$mensaje_error.=' -Liquidación se encuentra duplicada en el archivo de carga ';
						}

						//->Validación 2: Validando nombres y apellido del paciente
						if(trim($pac_nombre)=='' || trim($arreglo_lectura["D"].' '.$arreglo_lectura["F"]=='')){
							$logger->debug("--- Error Formato dato: (Tabla Pacientes), no poseee nombre y apellido; reprueba validación:".$row); 
							$mensaje_error.='- Nombre de paciente especificado incorrectamente, mínimo apellido paterno+nombre y no pueden ser nulos';
						}
						
						//->Validación 3: RUT no puede estar vacío
						if($rut_id == null || $rut_id ==''){
							$logger->debug("--- Error Formato dato: (Tabla Pacientes), no poseee rut; reprueba validación:".$row); 
							$mensaje_error.='- RUT no puede ser una cadena vacía';
							$rut_id=""; //podría ocasionar problemas por ser una primary key de otras tablas							
						}

						//->Validación 4: Referente a la liquidación
						if(!is_numeric($liq_saldo) || $liq_saldo == '' ){							
							$logger->debug("--- Error Formato dato: (Tabla Liquidacion), saldo no es un número; reprueba validación:".$row); 
							$mensaje_error.='- Monto de liquidación (saldo) respectiva no puede ser vacía y debe ser un número';
							$liq_saldo= 0;
						}
						
						//->Validación 5: Referente a los días deuda
						if(!is_numeric($dias_deuda) || $dias_deuda == ''){
							$logger->debug("--- Error Formato dato: (Tabla Liquidacion) dias_deuda no es numerico o es nulo, reprueba validación:".$row); 
							$mensaje_error.='- Los días de deuda es un valor obligatorio, debe ser un número y no puede quedar vacía';
							$dias_deuda = 0;
						} 	

						//->Validaciones 6: Referente a liquidacion_id no pueden ser nula o distinta de sus formatos
						if(!is_numeric($liquidacion_id) || $liquidacion_id==null){
							$logger->debug("--- Error Formato dato: (Tabla Liquidacion) liquidación es clave primaria, no es numerico o es nulo, reprueba validación:".$row); 
							$mensaje_error.='- liquidación_id no puede ser vacío y distinto de un número';
							$liquidacion_id = 0;
						}
												
						//->Validación 7: El id de ingreso no puede ser nulo
						if(trim(($ingreso_id))=='' || $ingreso_id==null){
							$logger->debug("--- Error Formato dato: (Tabla Ingresos) ingreso es clave primaria, no debe ser nulo ni estar vacío, reprueba validación:".$row); 
							$mensaje_error.='- ingreso_id no puede ser vacío y distinto de un número';
							$ingreso_id = "";
						}

						//->Validación 8: Isapre
						if(trim($ing_prevision) == '')
							$mensaje_error.='- La previsión del paciente no puede ser nula';

						//Validaciones opcionales que no restringen la ejecución de la aplicación
						/* if(!is_numeric($user_id)) {		
							$user_id			= 9;
							$logger->debug("--- Error Formato dato: (Tabla Pacientes) Ejecutivo de Corbanza no es numerico, se deja vacio:".$row); 
						} */

						if(!valida_rut($rut_id)){
							$logger->debug("--- El rut del paciente no cumple la validación, pero será ingresado de todas formas al sistema:".$rut_id." :".$row);
						}
						
						//Validando rut de girador
						if(!valida_rut($rut_girador))
							$logger->debug("--- El rut del girador no cumple validación pero será registrado:".$rut_girador." :".$row);


						//--> Chequeando datos de fecha
						$ing_fecha_admision_ =explode("'",$ing_fecha_admision);
						$_ing_fecha_admision_ =$ing_fecha_admision_[1] ? $ing_fecha_admision_[1] : $arreglo_lectura["N"];
						if(validateDate($_ing_fecha_admision_)!=true){
							$logger->debug("--- Error Formato dato: (Tabla Ingresos) ing_fecha_admision no es tipo fecha, pero será registrado:".$row);
							$ing_fecha_admision=$_ing_fecha_admision_;
						}
						$ing_fecha_alta_ =explode("'",$ing_fecha_alta);
						$_ing_fecha_alta_ = $ing_fecha_alta_[1] ? $ing_fecha_alta_[1] : $arreglo_lectura["O"];
						if(validateDate($_ing_fecha_alta_)!=true){
							$logger->debug("--- Error Formato dato: (Tabla Ingresos) ing_fecha_alta no es tipo fecha, pero será registrado:".$row);
							$ing_fecha_alta = $_ing_fecha_alta_;
						}
													

						$bool_validacion = $mensaje_error == ''; //Si no hay errores entregará un true, sino un false
						
						if($bool_validacion)
						{
							//--> Paso 4.4 INSERTAR REGISTROS EN TABLAS RESPECTIVAS
							//Asignación de las EC
							$user_id = $coleccion_ce[$index_ce];
							if(($index_ce+1) >= count($coleccion_ce))
								$index_ce =0; //Por desborde se resetea el valor
								else
							$index_ce +=1; //Incrementando el valor

							//--> Paso 4.4.1 Insertar PACIENTES --------------------------------------------------------------------------------
							//Estableciendo las reglas de negocio para los teléfonos
							$sql_paciente_exists = 	"select exists(select rut 
													from dav_pacientes
													where 
													rut		= '".$rut_id."' 
													) as resultado 
													";
							//La query da true=t y false=f
							$rs_paciente_exists  	= pg_query($con,$sql_paciente_exists);
							$Result 	  	  		= pg_fetch_assoc($rs_paciente_exists);
							$tabla 					= "";
	
							if($Result['resultado']=="t"){
								$tabla 				= "dav_pacientes_solucion_mod"; //-->Existe en la tabla
							} else {
								$tabla 				= "dav_pacientes_solucion_ins"; //--> No existe en la tabla
							}	
							//--> Chequeando en solucion
							$sql_paciente_exists_sol = 	"select exists(select rut 
													from dav_pacientes_solucion_ins
													where 
													rut		= '".$rut_id."' 
													) as resultado 
													";
	
							$rs_paciente_exists_sol = pg_query($con,$sql_paciente_exists_sol);
							$Result_sol	  	  		= pg_fetch_assoc($rs_paciente_exists_sol);
							$tabla_sol				= "";

							if($Result_sol['resultado']=="t"){
								//-->Existe en la tabla que viene en la misma carga, por lo que no se cargará 2 veces							
							} else {
								//--> No existe en la tabla
								$sql_ins_paciente 		= "insert into ".$tabla." 
													(rut,carga_id,user_id,pac_nombre,pac_telefono1,pac_telefono2,pac_celular1,pac_celular2,created,modified,pac_activo) 
													values 
													('$rut_id',$carga_id,$user_id,'$pac_nombre','$pac_telefono1','$pac_telefono2','$pac_celular1','$pac_celular2','$created',
													'$modified',$pac_activo)
													";
	
								$rs_ins_paciente  = pg_query($con,$sql_ins_paciente);	
							}	
							//--> Paso 4.4.2 Insertar INGRESOS --------------------------------------------------------------------------------
							$sql_ingreso_exists = 	"select exists(select id, rut_id  
													from dav_ingresos 
													where 
													id 		= '".$ingreso_id."' 
													and rut_id		= '".$rut_id."' 
													) as resultado 
													";
	
							$rs_ingreso_exists  	= pg_query($con,$sql_ingreso_exists);
							$Result 	  	  		= pg_fetch_assoc($rs_ingreso_exists);
							$tabla 					= "";
	
							if($Result['resultado']=="t"){
								$tabla 				= "dav_ingresos_solucion_mod"; //-->Existe en la tabla
							} else {
								$tabla 				= "dav_ingresos_solucion_ins"; //--> No existe en la tabla
							}	
							//--> Chequeando en solucion
							$sql_ingreso_exists_sol = 	"select exists(select id, rut_id 
													from dav_ingresos_solucion_ins 
													where
													id 				= '".$ingreso_id."'  
													and rut_id		= '".$rut_id."' 
													) as resultado 
													";
	
							$rs_ingreso_exists_sol = pg_query($con,$sql_ingreso_exists_sol);
							$Result_sol	  	  		= pg_fetch_assoc($rs_ingreso_exists_sol);
							$tabla_sol				= "";

							if($Result_sol['resultado']=="t"){
								//-->Existe en la tabla que viene en la misma carga, por lo que no se cargará 2 veces							
							} else {
								//--> No existe en la tabla
								$sql_val_final ="select exists(select id, rut_id 
													from ".$tabla." 
													where
													id 				= '".$ingreso_id."' 
													and rut_id		= '".$rut_id."' 
													) as resultado 
													";
	
								$rs_val_final  	= pg_query($con,$sql_val_final);
								$Result_final	= pg_fetch_assoc($rs_val_final);
								if($Result_final['resultado']=="t"){
									//--> Existe en la tabal de paso, por lo que no se hace nada
								} else {		
									$sql_ins_ingreso 	= "insert into ".$tabla." 
														(
														id,rut_id,carga_id,created,modified,ing_activo,ing_prevision,
														ing_fecha_admision,ing_fecha_alta,rut_girador,fono_girador,
														nombre_girador, ing_estado, diagnostico_egreso, motivo_admision, saldo_total, ing_estado_alta  
														) 
														values 
														(
														'$ingreso_id','$rut_id',$carga_id,'$created','$modified',$ing_activo,$ing_prevision,
														$ing_fecha_admision,$ing_fecha_alta,$rut_girador,$fono_girador,													
														$nombre_girador, $ing_estado, $diagnostico_egreso, $motivo_admision, $saldo_total, '$ing_estado_alta'  
														)";

									$rs_ins_ingreso  = pg_query($con,$sql_ins_ingreso);
								}	
							}	
							//--> Paso 4.4.3 Insertar LIQUIDACIONES --------------------------------------------------------------------------------
							$sql_liquida_exists = 	"select exists(select id, rut_id, ingreso_id  
													from dav_liquidacions  
													where 
													id 				= ".$liquidacion_id."   
													and rut_id		= '".$rut_id."' 
													and ingreso_id  = '".$ingreso_id."'  
													) as resultado 
													";
	
							$rs_liquida_exists  	= pg_query($con,$sql_liquida_exists);
							$Result 	  	  		= pg_fetch_assoc($rs_liquida_exists);
							$tabla 					= "";
	
							if($Result['resultado']=="t"){
								$tabla 				= "dav_liquidacions_solucion_mod"; //-->Existe en la tabla
							} else {
								$tabla 				= "dav_liquidacions_solucion_ins"; //--> No existe en la tabla
							}	
	
							//--> Chequeando en solucion
							$sql_liquida_exists_sol = 	"select exists(select id, rut_id, ingreso_id 
													from dav_liquidacions_solucion_ins  
													where
													id 				= '".$liquidacion_id."' 
													and rut_id		= '".$rut_id."' 
													and ingreso_id  = '".$ingreso_id."'
													) as resultado 
													";
	
							$rs_liquida_exists_sol 	= pg_query($con,$sql_liquida_exists_sol);
							$Result_sol	  	  		= pg_fetch_assoc($rs_liquida_exists_sol);
							$tabla_sol				= "";
							
							if($Result_sol['resultado']=="t"){
								//-->Existe en la tabla que viene en la misma carga, por lo que no se cargará 2 veces							
							} else {
								//--> No existe en la tabla
								$sql_ins_liquida 	= "insert into ".$tabla." 
													(
													id,rut_id,ingreso_id,carga_id,created,modified,liq_activo,liq_garantia,liq_tiporiesgo,
													liq_copagoclinica, liq_copagohhmm,liq_saldo,liq_saldogirador, dias_deuda, 
													fecha_envio_isapre, fecha_entrega_ejecutivo, fecha_entrega_isapre
													) 
													values 
													(
													$liquidacion_id,'$rut_id','$ingreso_id',$carga_id,'$created','$modified',$liq_activo,$liq_garantia,$liq_tiporiesgo,
													$liq_copagoclinica,$liq_copagohhmm,$liq_saldo,$liq_saldogirador, $dias_deuda, 
													$fecha_envio_isapre, $fecha_entrega_ejecutivo, $fecha_entrega_isapre 
													)
													";
	
								//echo $sql_ins_liquida;
								$rs_ins_liquida  = pg_query($con,$sql_ins_liquida);	
							}	

							//--> Paso 4.4.5 Insertar DATOS AUXILIARES --------------------------------------------------------------------------------								
							$sql_ins_aux 	= "insert into dav_auxiliar_carga (carga_id, rut_id, liquidacion_id, 
														unidad_hosp_ingreso, pam, fecha_cierre, clasificador, 
														identificador ) values (".$carga_id.", '".$rut_id."', 
														".$liquidacion_id.", '".$unidad_hosp_ingreso."', '".$pam."', 
														'".$fecha_cierre."', '".$clasificador."', '".$rango."')";

							$rs_ins_aux  = pg_query($con,$sql_ins_aux);	
							//Validación e inserción telefónica se verá fuera de estas validaciones "primarias"

							//Finalmente se agrega al contador un registro correcto
							$cuentaregistros_correctos +=1;
							array_push($registro_correcto, $row-1);

						} 
						else //Ahora qué ocurre si hay duplicados¿? O si no hay  validaciones que no se respetan
						{

							$user_id="0"; //No se asigna ninguna EC en caso de error
							//--> Paso 4.4.1 Insertar DATOS PACIENTES MODO ERROR--------------------------------------------------------------------------------	
							$logger->debug("- Registro tabla dav_pacientes_error de linea de archivo carga ".$row."");
							$sql_paciente_exists_error = "select exists(select rut 
													from dav_pacientes_error 
													where rut = '".$rut_id."' and carga_id = $carga_id) as resultado";

							$rs_paciente_exists_error = pg_query($con,$sql_paciente_exists_error);
							$resultado_paciente_error = pg_fetch_assoc($rs_paciente_exists_error);

							if($resultado_paciente_error['resultado']=="f"){ //Sólo si no existe se inserta
								
								$sql_ins_paciente_error = "insert into dav_pacientes_error (rut,carga_id,user_id,pac_nombre, 
															pac_telefono1,pac_telefono2,pac_celular1,pac_celular2,created,modified,
															pac_activo)	values ('$rut_id',$carga_id,$user_id,'$pac_nombre',
															'$pac_telefono1','$pac_telefono2','$pac_celular1', '$pac_celular2',
															'$created','$modified',$pac_activo)";
	
								$rs_ins_paciente_error = pg_query($con,$sql_ins_paciente_error);

							}

							//--> Paso 4.4.2 Insertar DATOS INGRESOS MODO ERROR--------------------------------------------------------------------------------	
							$logger->debug("- Registro tabla dav_ingresos_error de linea de archivo carga ".$row."");

							$sql_ingreso_exists_error = "select exists(select id, rut_id 
													from dav_ingresos_error 
													where
													id 				= '".$ingreso_id."'  
													and rut_id		= '".$rut_id."' 
                          							and carga_id 	= $carga_id 
													) as resultado 
													";

							$rs_ingreso_exists_error = pg_query($con,$sql_ingreso_exists_error);
							$resultado_ingreso_error = pg_fetch_assoc($rs_ingreso_exists_error);

							if($resultado_ingreso_error['resultado']=="f"){
															
								$sql_ins_ingreso_error 	= "insert into dav_ingresos_error (	id,rut_id,carga_id,created,modified,ing_activo,
														ing_prevision, ing_fecha_admision,ing_fecha_alta,rut_girador,fono_girador,
														nombre_girador, ing_estado, diagnostico_egreso, motivo_admision, saldo_total, 
														ing_estado_alta) values (
														'$ingreso_id','$rut_id',$carga_id,'$created','$modified',$ing_activo,$ing_prevision,
														$ing_fecha_admision,$ing_fecha_alta,$rut_girador,$fono_girador,													
														$nombre_girador, $ing_estado, $diagnostico_egreso, $motivo_admision, $saldo_total, 
														'$ing_estado_alta')";

								$rs_ins_ingreso_error  = pg_query($con,$sql_ins_ingreso_error);

							}
							
							//--> Paso 4.4.3 Insertar DATOS LIQUIDACIONES MODO ERROR--------------------------------------------------------------------------------	
							$logger->debug("- Registro tabla dav_liquidacions_error de linea de archivo carga ".$row."");
							$sql_ins_liquida_error 	= "insert into dav_liquidacions_error (
													id,rut_id,ingreso_id,carga_id,created,modified,liq_activo,liq_garantia,liq_tiporiesgo,
													liq_copagoclinica, liq_copagohhmm,liq_saldo,liq_saldogirador, dias_deuda, 
													fecha_envio_isapre, fecha_entrega_ejecutivo, fecha_entrega_isapre, mensaje_error 
													) values (
													$liquidacion_id,'$rut_id','$ingreso_id',$carga_id,'$created','$modified',$liq_activo,$liq_garantia,$liq_tiporiesgo,
													$liq_copagoclinica,$liq_copagohhmm,$liq_saldo,$liq_saldogirador, $dias_deuda, 
													$fecha_envio_isapre, $fecha_entrega_ejecutivo, $fecha_entrega_isapre, '$mensaje_error'   
													)";

							$rs_ins_liquida  = pg_query($con,$sql_ins_liquida_error);	

							//--> Paso 4.4.4 Insertar DATOS AUXILIARES MODO ERROR--------------------------------------------------------------------------------	
							$logger->debug("- Registro tabla dav_auxiliar_carga_error de linea de archivo carga ".$row."");                                               
							$sql_auxiliar_exists_error = "select exists(select id, rut_id from dav_auxiliar_carga_error where
														ingreso_id 			= '".$ingreso_id."'  
														and rut_id			= '".$rut_id."' 
														and liquidacion_id 	= ".$liquidacion_id." and carga_id = ".$carga_id." ) as resultado";

							$rs_auxiliar_exists_error = pg_query($con,$sql_auxiliar_exists_error);
							$resultado_auxiliar_error = pg_fetch_assoc($rs_auxiliar_exists_error);
																									
							if($resultado_auxiliar_error['resultado']=="f"){

								$sql_ins_aux_error 	= "insert into dav_auxiliar_carga_error (carga_id, rut_id, liquidacion_id, 
														unidad_hosp_ingreso, pam, fecha_cierre, clasificador, 
														identificador ) values (".$carga_id.", '".$rut_id."', 
														".$liquidacion_id.", '".$unidad_hosp_ingreso."', '".$pam."', 
														'".$fecha_cierre."', '".$clasificador."', '".$rango."')";
	
								$rs_ins_aux_error  = pg_query($con,$sql_ins_aux_error); //Inserción tabla auxiliar de errores lista
							}

							//Finalmente, insertamos los contadores
							$cuentaregistros_error += 1;
							array_push($registro_error, $row-1);
							$logger->debug("--- Este registro número de fila ".strval($row)." tiene errores: ".$mensaje_error);
							//Debido a que existe al menos, un error, se procede a crear los repositorios para generar archivos xlsx con los errores
							if($cuentaregistros_error == 1)
							{
								if(!is_dir($path_proyecto."errores/")){
									mkdir($path_proyecto."errores/",0777);
								}
								if(!is_dir($path_adjunto.$fecha_carpeta_adjuntos."/")){
									mkdir($path_adjunto.$fecha_carpeta_adjuntos."/",0777);
								}
							}

						} //fin del if que aprueba las validaciones
																				
						//--> Paso 4.4.5 Insertar TELEFONIA FUERA DE LAS VALIDACIONES--------------------------------------------------------------------------------
						
						//Experimento para insertar números a la BD integral
						//Ejecución del experimento: para insertar números nuevos a la bd integral
						if($arreglo_lectura["H"] != "0" && $arreglo_lectura["H"] != "" && $arreglo_lectura["H"] != NULL)
							$resp_insercion_fono[$rut_id][$arreglo_lectura["H"]] = Biblioteca::insertTelefono_servicio_cURL($rut_id, 2, $arreglo_lectura["H"]);
						if($arreglo_lectura["I"] != "0" && $arreglo_lectura["I"] != "" && $arreglo_lectura["I"] != NULL)
							$resp_insercion_fono[$rut_id][$arreglo_lectura["I"]] = Biblioteca::insertTelefono_servicio_cURL($rut_id, 2, $arreglo_lectura["I"]);	
						if($arreglo_lectura["J"] != "0" && $arreglo_lectura["J"] != "" && $arreglo_lectura["J"] != NULL)
							$resp_insercion_fono[$rut_id][$arreglo_lectura["J"]] = Biblioteca::insertTelefono_servicio_cURL($rut_id, 2, $arreglo_lectura["J"]);	
						if($arreglo_lectura["K"] != "0" && $arreglo_lectura["K"] != "" && $arreglo_lectura["K"] != NULL)
							$resp_insercion_fono[$rut_id][$arreglo_lectura["K"]] = Biblioteca::insertTelefono_servicio_cURL($rut_id, 2, $arreglo_lectura["K"]);	
						//fin experimento
						//cod_fono 1: FiJO; 2: CELULAR: 

						//Hay casos que los celulares se duplican en las columnas, por ello se trabajará en las prioridades de esta manera
						$prioridad = 1; //Contador que servirá para establecer la prioridad real de la inserción
						//Se completan las tablas auxiliares solucion_mod y solucion_ins
						//Celulares prioridades
						if($arreglo_lectura["J"]==$arreglo_lectura["K"]){  //J dbeería ser prioridad 1 y K la segunda
							if($arreglo_lectura["J"] != "0" && $arreglo_lectura["J"] != "" && $arreglo_lectura["J"] != NULL){
								insertarTelefonia($rut_id,$ingreso_id,$carga_id,$arreglo_lectura["J"],$prioridad,2,$created,$modified,$con);
								$prioridad +=1;
							}
						} else {
							if($arreglo_lectura["J"] != "0" && $arreglo_lectura["J"] != "" && $arreglo_lectura["J"] != NULL){
								insertarTelefonia($rut_id,$ingreso_id,$carga_id,$arreglo_lectura["J"],$prioridad,2,$created,$modified,$con); 	//--> Prioridad 1 celular paciente	
								$prioridad +=1;
							}
							if($arreglo_lectura["K"] != "0" && $arreglo_lectura["K"] != "" && $arreglo_lectura["K"] != NULL){
								insertarTelefonia($rut_id,$ingreso_id,$carga_id,$arreglo_lectura["K"],$prioridad,2,$created,$modified,$con); 
								$prioridad +=1;
							}
						}
						//Red fija prioridades
						if($arreglo_lectura["H"]==$arreglo_lectura["I"]){
							if($arreglo_lectura["H"] != "0" && $arreglo_lectura["H"] != "" && $arreglo_lectura["H"] != NULL){
								insertarTelefonia($rut_id,$ingreso_id,$carga_id,$arreglo_lectura["H"],$prioridad,1,$created,$modified,$con);
								$prioridad +=1;
							}
						} else {
							if($arreglo_lectura["H"] != "0" && $arreglo_lectura["H"] != "" && $arreglo_lectura["H"] != NULL){
								insertarTelefonia($rut_id,$ingreso_id,$carga_id,$arreglo_lectura["H"],$prioridad,1,$created,$modified,$con); 	//--> Prioridad 3 fono paciente
								$prioridad +=1;
							}
							if($arreglo_lectura["I"] != "0" && $arreglo_lectura["I"] != "" && $arreglo_lectura["I"] != NULL){
								insertarTelefonia($rut_id,$ingreso_id,$carga_id,$arreglo_lectura["I"],$prioridad,1,$created,$modified,$con); 
								$prioridad +=1;
							}
						}						

					}

					//--> Paso 5: TRASPASANDO los datos a tablas originales EDIT
					$logger->debug("Paso 5: TRASPASANDO los datos a tablas originales EDIT");

					//--> 	Paso 6.1: DATOS PACIENTE
					$sql_paciente = 	"UPDATE dav_pacientes 
										SET 
										rut 			= subquery.rut,  
										carga_id 		= subquery.carga_id,
										user_id 		= subquery.user_id, 
										pac_nombre 		= subquery.pac_nombre, 
										pac_telefono1 	= subquery.pac_telefono1,
										pac_telefono2   = subquery.pac_telefono2,
										pac_celular1	= subquery.pac_celular1,
										pac_celular2	= subquery.pac_celular2,
										modified 		= subquery.modified, 
										pac_activo 		= subquery.pac_activo 
										from (SELECT * from dav_pacientes_solucion_mod) as subquery 
										where  
										dav_pacientes.rut=subquery.rut 
										";

					$rs_paciente  = pg_query($con,$sql_paciente);
					$logger->debug("- Tabla PACIENTE EDIT");


					//--> 	Paso 6.2: DATOS INGRESOS
					$sql_ingresos_mod 	= 	"UPDATE dav_ingresos 
										SET 
										carga_id 			= subquery.carga_id, 
										modified 			= subquery.modified, 
										ing_activo 			= subquery.ing_activo, 
										ing_recaudador 		= subquery.ing_recaudador, 
										ing_prevision 		= subquery.ing_prevision, 
										ing_estado_alta 	= subquery.ing_estado_alta, 
										ing_edad 			= subquery.ing_edad, 
										ing_seguro_compl	= subquery.ing_seguro_compl, 
										ing_fecha_admision 	= subquery.ing_fecha_admision, 
										ing_fecha_alta 		= subquery.ing_fecha_alta, 
										ing_reclamo_sac 	= subquery.ing_reclamo_sac, 
										rut_girador 		= subquery.rut_girador, 
										fono_girador 		= subquery.fono_girador, 
										nombre_girador      = subquery.nombre_girador, 
										ing_estado 			= subquery.ing_estado, 
										diagnostico_egreso	= subquery.diagnostico_egreso, 
										motivo_admision		= subquery.motivo_admision, 
										saldo_total			= subquery.saldo_total 
										from (SELECT * from dav_ingresos_solucion_mod) as subquery 
										where 
										dav_ingresos.rut_id = subquery.rut_id 
										and dav_ingresos.id = subquery.id 
										";

					$rs_ingresos  = pg_query($con,$sql_ingresos_mod);
					$logger->debug("- Tabla INGRESOS EDIT");


					//--> 	Paso 6.3: DATOS LIQUIDACIONES
					$sql_liquidacion_mod = 	"UPDATE dav_liquidacions 
										SET 
										carga_id  				= subquery.carga_id, 
										modified  				= subquery.modified, 
										liq_activo  			= subquery.liq_activo, 
										liq_saldo  				= subquery.liq_saldo, 
										dias_deuda				= subquery.dias_deuda, 
										fecha_envio_isapre		= subquery.fecha_envio_isapre, 
										fecha_entrega_ejecutivo	= subquery.fecha_entrega_ejecutivo, 
										fecha_entrega_isapre	= subquery.fecha_entrega_isapre 
										from (SELECT * from dav_liquidacions_solucion_mod) as subquery 
										where 
										dav_liquidacions.rut_id 		= subquery.rut_id
										and dav_liquidacions.ingreso_id = subquery.ingreso_id 
										and dav_liquidacions.id  		= subquery.id 
										";
					
					$rs_liquidacion  = pg_query($con,$sql_liquidacion_mod);
					$logger->debug("- Tabla LIQUIDACION EDIT");


					//-->	Paso 6.4: DATOS TELEFONIA
					$sql_telefonia_mod   = 	"UPDATE dav_telefonia 
										SET 
										carga_id  			= subquery.carga_id, 
										modified  			= subquery.modified, 
										cod_fono  			= subquery.cod_fono, 
										cod_area  			= subquery.cod_area, 
										fono_error  		= subquery.fono_error, 
										origen  			= subquery.origen, 
										viene_hoy  			= subquery.viene_hoy, 
										prioridad  			= subquery.prioridad 
										from (SELECT * from dav_telefonia_solucion_mod) as subquery 
										where 
										dav_telefonia.rut_id 		= subquery.rut_id and 
										dav_telefonia.ingreso_id    = subquery.ingreso_id and 
										dav_telefonia.numero_fono   = subquery.numero_fono 
										";

					$rs_telefonia  = pg_query($con,$sql_telefonia_mod);
					
					$logger->debug("- Tabla TELEFONIA EDIT");
					 

				//--> Paso 6: TRASPASANDO los datos a tablas originales INSERT
					$logger->debug("Paso 5: TRASPASANDO los datos a tablas originales INSERT");

					//--> 	Paso 7.1: DATOS PACIENTE
					$sql_volcado_paciente = "INSERT INTO dav_pacientes  
						(
						rut,carga_id,user_id,pac_nombre,pac_telefono1,pac_telefono2,pac_celular1,pac_celular2,created,modified,pac_activo
						) 
						(
						SELECT 
						rut,carga_id,user_id,pac_nombre,pac_telefono1,pac_telefono2,pac_celular1,pac_celular2,created,modified,pac_activo
						FROM dav_pacientes_solucion_ins  
						)
						";

					$rs_volcado_paciente = pg_query($con,$sql_volcado_paciente);
					$logger->debug("- TABLA PACIENTE INSERT");

					
					//--> 	Paso 7.2: DATOS INGRESOS
					$sql_volcado_ingresos = "INSERT INTO dav_ingresos 
						(
						id,rut_id,carga_id,created,modified,ing_activo,ing_recaudador,ing_prevision,ing_estado_alta,
						ing_edad,ing_seguro_compl,ing_fecha_admision,ing_fecha_alta,ing_reclamo_sac,rut_girador,fono_girador,
						nombre_girador, ing_estado, diagnostico_egreso, motivo_admision, saldo_total 
						)
						(
						SELECT 
						a.id,a.rut_id,a.carga_id,a.created,a.modified,a.ing_activo,a.ing_recaudador,a.ing_prevision,a.ing_estado_alta,
						a.ing_edad,a.ing_seguro_compl,a.ing_fecha_admision,a.ing_fecha_alta,a.ing_reclamo_sac,a.rut_girador,a.fono_girador,
						a.nombre_girador, a.ing_estado, a.diagnostico_egreso, a.motivo_admision, a.saldo_total   
						FROM dav_ingresos_solucion_ins a left join dav_ingresos b on a.id=b.id and a.rut_id=b.rut_id where b.id is null
						)
						";
					
					$rs_volcado_ingresos = pg_query($con,$sql_volcado_ingresos);
					$logger->debug("- TABLA INGRESOS INSERT");


					//--> 	Paso 7.3: DATOS LIQUIDACIONES

				//14-06-2018 Modificacion po Mpoblete , se incorpora nueva llave primaria (id_rut_id,ingreso_id) a tabla liquidaciones, y se cambia el insert	
				$sql_volcado_liquidacion = "INSERT INTO dav_liquidacions
									(
										id,rut_id,ingreso_id,carga_id,created,modified,liq_activo,liq_saldo, dias_deuda,
										fecha_envio_isapre, fecha_entrega_ejecutivo, fecha_entrega_isapre 
									)
									(
										SELECT 
										a.id,a.rut_id,a.ingreso_id,a.carga_id,a.created,a.modified,a.liq_activo, a.liq_saldo,
										a.dias_deuda, a.fecha_envio_isapre, a.fecha_entrega_ejecutivo, a.fecha_entrega_isapre
										FROM dav_liquidacions_solucion_ins a left join dav_liquidacions b on a.id=b.id and a.rut_id=b.rut_id and a.ingreso_id=b.ingreso_id
										where b.id is null
									)";


					$rs_volcado_liquidacion = pg_query($con,$sql_volcado_liquidacion);
					$logger->debug("- TABLA LIQUIDACIONES INSERT");


					//-->	Paso 7.4: DATOS TELEFONIA
					$sql_volcado_telefonia = "INSERT INTO dav_telefonia 
									(
									rut_id,ingreso_id,carga_id,created,modified,cod_fono,cod_area,numero_fono,telefonia_activo,fono_error,origen,viene_hoy,prioridad 
									)
									(
									SELECT 
									rut_id,ingreso_id,carga_id,created,modified,cod_fono,cod_area,numero_fono,telefonia_activo,fono_error,origen,viene_hoy,prioridad 
									FROM dav_telefonia_solucion_ins  
									)
									";
					
					//echo $sql_volcado_telefonia; 

					$rs_volcado_telefonia = pg_query($con,$sql_volcado_telefonia);
					$logger->debug("- TABLA TELEFONIA INSERT");

				//--> Paso 7: Cerrando la carga de datos
					//--> Cerrando la carga
					$logger->debug("Cerrando la Carga de datos");
					$fec_fincarga =date('Y-m-d H:i:s'); 

					$sql_update_carga_mdb=	"UPDATE cargas 
											set car_fechafin ='$fec_fincarga', 
											car_estado = 'Finalizado'  
											where id = 	$id_nueva_carga 
											";

					$rs_nuevacarga	= pg_query($con,$sql_update_carga_mdb);


					$logger->debug("Activando los Teléfonos cargados por fuera de DAVILA. ");
					//echo date('Y-m-d h:i:s')."Activando los telefonos cargados por fuera de Santander \n";
					$sql_valida1 = "update dav_telefonia set modified='".date('Y-m-d h:m:s')."', carga_id=".$id_nueva_carga." where origen in (2,3,4,5)";
					$rs_valida1  = pg_query($con,$sql_valida1);
					
					//--> Paso 8: Generando email de FInalizacion de Carga
					try{		
						//--> Datos de la carga
						$date_actual=date('Y-m-d');
						$sql_pacientes_procesados 	= "select count(*) from dav_pacientes where carga_id=".$id_nueva_carga." ";
						$rs_pacientes_procesados	= pg_query($con,$sql_pacientes_procesados);
						$row_pacientes_procesados	= pg_fetch_row($rs_pacientes_procesados); 
						$cantidad_pacientes			= $row_pacientes_procesados['0'];

						$sql_ingresos_procesados 	= "select count(*) from dav_ingresos where carga_id=".$id_nueva_carga." ";
						$rs_ingresos_procesados		= pg_query($con,$sql_ingresos_procesados);
						$row_ingresos_procesados	= pg_fetch_row($rs_ingresos_procesados); 
						$cantidad_ingresos			= $row_ingresos_procesados['0'];

						$sql_liquida_procesados   	= "select count(*) from dav_liquidacions where carga_id=".$id_nueva_carga." ";
						$rs_liquida_procesados		= pg_query($con,$sql_liquida_procesados);
						$row_liquida_procesados		= pg_fetch_row($rs_liquida_procesados); 
						$cantidad_liquida			= $row_liquida_procesados['0'];


						$cuerpo='<html>
							<head><meta http-equiv="Content-Type" content="text/html; charset=windows-1252"><title>Carga de datos</title></head>
			 				<body>
			 					<b>Estimados:</b><br>
								Buenos d&iacute;as. Comunico que se ha ejecutado el nuevo proceso de carga de datos con la informaci&oacute;n depositada por D&aacute;vila hoy <b>'.date('d-m-Y').'</b><br><br>
								
								A continuaci&oacute;n tabla detalle de Carga:<br>
								<table border="1" colspacing="0" collpading="0" width="80%">
									<tr>
										<td width="20%">Carga ID:</td>
										<td width="40%"><b>'.$id_nueva_carga.'</b></td>
									</tr>
									<tr>
										<td width="20%">Archivo Cargado:</td>
										<td width="40%"><b>'.$archivo_datos.'</b></td>
									</tr>
									<tr>
										<td>Inicio Carga:</td>
										<td><b>'.$fecha_nueva_carga.'</b></td>
									</tr>
									<tr>
										<td>Termino Carga:</td>
										<td><b>'.$fec_fincarga.'</b></td>
									</tr>
									<tr>
										<td>Lineas Procesadas</td>
										<td><b>'.($highestRow-1).'</b></td>
									</tr>
								</table>
								<br><br>
								<table border="1" colspacing="0" collpading="0">
									<tr>
										<td width="20%">Pacientes Procesados:</td>
										<td><b>'.$cantidad_pacientes.'</b></td>
									</tr>
									<tr>
										<td>Ingresos Procesados:</td>
										<td><b>'.$cantidad_ingresos.'</b></td>
									</tr>
									<tr>
										<td>Liquidaciones Procesadas</td>
										<td><b>'.$cantidad_liquida.'</b></td>
									</tr>
								</table>
								<br>';
								//Mostrando el detalle de errores... si existiesen
								if($cuentaregistros_error > 0){
									//Descomente esto para mostrar el detalle en el cuerpo del correo
									/*$sql_error_registros = "select rut_id as rut, ingreso_id as ingreso, id as liquidacion, mensaje_error as mensaje
															from dav_liquidacions_error where carga_id='$id_nueva_carga'"; 
									$query_error_registros = $rs_ins_liquida  = pg_query($con,$sql_error_registros);	

									$cuerpo.='<h4>Casos de errores</h4><br>
												<table border="1" colspacing="0" collpading="0">
												<thead><tr>
													<th>RUT</th>
													<th>N. Ingreso</th>
													<th>N. Liquidacion</th>
													<th>Error(es)</th>
												<tr></thead>
												<tbody>';
									while($fila=pg_fetch_object($query_error_registros)){
										$cuerpo.='<tr>
													<td>'.$fila->rut.'</td>
													<td>'.$fila->ingreso.'</td>
													<td>'.$fila->liquidacion.'</td>
													<td>'.utf8_decode($fila->mensaje).'</td>
													</tr>';
									}
									$cuerpo.='</tbody></table><br><br>';
									*/									
									//Descomentar para enviar archivo xlsx. Recuerde descomentar más abajo en apartado de adjuntos
									$i=0;
                  					$attachExcel=array();
                  					$arrAdjuntos=array();
									foreach($arreglo_isapre as $isapres){ 
										$attachExcel[$i] = GeneraExcel($isapres);
                    					$i+=1;
									}
						  									
									for($j=0;$j<count($attachExcel);$j++){
										$arrAdjuntos[$attachExcel[$j]] = $path_adjuntos.$attachExcel[$j];
									}
								}						

								$cuerpo.='Quedo atento a comentarios<br>
								Saludos cordiales<br>
								<b>Administrador Sistema COBRA 2080sys<br>para Clinica Davila.</b>	
			 				</body>
			 			</html>';
			
						
			 			$mail = new PHPMailer();			
						$mail->isSMTP();            
						$mail->Host = "ssl://mail.2080.cl";
						$mail->Port = 465; 
						$mail->SMTPAuth = true;                           
						$mail->Username =  "cobra@2080.cl";                            
						$mail->Password = "N25VLHx.zM";                                                
						$mail->SMTPSecure = "SSl";                                                 			
						$mail->From = "cobra@2080.cl";
						$mail->FromName = "Adm.Cobra 2080 - Davila";
						$mail->addAddress("carlos.orrego@staff.2080.cl");
			 			$mail->addAddress("carmen.aguirre@2080.cl");
						$mail->addAddress("rodrigo.correa@2080.cl");
						$mail->addAddress("arturo.guillen@2080.cl"); 			
			 			$mail->isHTML(true);			
						//Descomentar si se usa el envío de archivo xlsx, recuerde descomentar además la parte del archivo xslx en el cuerpo del correo 
						//Se añadirá solamente si existen errores		
						if($cuentaregistros_error > 0){
							for($j=0;$j<count($attachExcel);$j++){
								$mail->AddAttachment($arrAdjuntos[$attachExcel[$j]]);
							  }
						}
						 
			 			$mail->Subject = "Clinica Davila: Carga de datos asociada al ".date('d-m-Y')." Finalizado.";				
			 			$mail->Body = $cuerpo;
			

			 			if(!$mail->send()) {
			 				echo "Mailer Error: " . $mail->ErrorInfo;
			 			}else {
			 				
			 				echo '
			 				<div class="mensaje-exito">
			 	    	        <p>Su mensaje a sido enviado con exito. Gracias!</p>
			 	        	</div>';
			 	        	
			 			}
				 	} catch (phpmailerException $e) {
				 		//echo $e->errorMessage(); //error messages from PHPMailer
				 		$logger->debug('Error Email: '.$e->errorMessage());
				 	} catch (Exception $e) {
				 		//echo $e->getMessage();
				 		$logger->debug('Error Email2: '.$e->getMessage());
				 	}

				 	$logger->debug("Finalizado envio de email.");

			} else {
				$logger->debug("No se efectuará la carga de datos porque porque no encontró el archivo en la carpeta de proceso".$id);
				$sw_ok_out = 0;
			}

		} else {
			$logger->debug("No se efectuará la carga de datos porque hoy ya existe una carga con anterioridad correspondiente a la carga".$id);
			$sw_ok_out = 0;
		}

		//--> TERMINADO EL PROCESO SE DEBE PASAR A PROCESADOS 
		$logger->debug("Carga de datos terminada, moviendo el archivo de la carpeta PROCESO a PROCESADOS");
		exec('cd '.$path_base.'cargaDatosDavila/proceso/; mv *.xlsx '.$path_base.'cargaDatosDavila/procesados/');
		exec('cd '.$path_base.'cargaDatosDavila/proceso/; mv *.txt '.$path_base.'cargaDatosDavila/procesados/');
	} 

	$logger->debug("Proceso Terminado");
	$logger->debug("******************************************************************");	

	//echo date('Y-m-d h:i:s')."Se termina el proceso \n";
	//Cierre de conexión
	pg_close($con);



/* Para recargas y testing

--Para recargas
delete from dav_liquidacions where carga_id		=235;
delete from dav_ingresos where carga_id			=235;
delete from dav_pacientes where carga_id		=235;
delete from dav_telefonia where carga_id		=235;
delete from cargas where id						=235;
delete from dav_auxiliar_carga where carga_id	=235;

delete from campanas where carga_id				=229;

select * from dav_liquidacions where carga_id	=238;
select * from dav_pacientes where carga_id		=238;
select * from dav_telefonia where carga_id		=238;
select * from dav_ingresos where carga_id		=238;
select * from cargas where id					=238;
select * from dav_auxiliar_carga where carga_id	=238;

select * from campanas where carga_id			=214;


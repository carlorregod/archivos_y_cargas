<?php
//Llamada a la clase de las conexiones
require_once('conexion.php');
//PARA LOS MÉTODOS DE PHPEXCEL
require_once('utils/PHPExcel/Classes/PHPExcel.php'); 
//Para correo electrónico
include_once('utils/script_cron/lib/class.phpmailer.php');
require_once('utils/script_cron/lib/class.smtp.php');

class Biblioteca{

    private static function validateDate_($date) {
    	if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$date)) {
   
	       return true;
	   }else{
	       return false;
	   }
    }
    
    //Función que validará los números de teléfono según reglas de negocio de Elastix
	private static function validaTelefono_($fono)
	{
		//Pre-corrección
		if(substr($fono,0,1)=="9" && strlen($fono)==9)
            $numero_fono = "0".$fono;
        elseif(substr($fono,0,1)=="9" && in_array(intval(substr($fono,1,1)),range(5,9)) && strlen($fono)==8)
            $numero_fono = "09".$fono;
        elseif(substr($fono,0,1)=="2" && substr($fono,1,1)!="0" && strlen($fono)==8)
            $numero_fono = "2".$fono;
        else
			$numero_fono = $fono;
		
		if(substr($numero_fono,0,2)=="09" || substr($numero_fono,0,1)=="9"){ //Celular
			if(substr($numero_fono,0,1)=="0")
			{
				$codigo_num =substr($numero_fono,0,2);
				$num_sin_codigo=substr($numero_fono,2);
			}
			else
			{
				$codigo_num ="0".substr($numero_fono,0,1);
				$num_sin_codigo=substr($numero_fono,1);
			}
			//A revisar las reglas de negocio del celular...  09XYYYZZZZ
			$numero_fono = $codigo_num.$num_sin_codigo;
			if(strlen($numero_fono)==10) {				
				if( intval(substr($numero_fono,0,1)) != 0 				|| 		//1=> cero
					intval(substr($numero_fono,1,1)) != 9 				|| 		//2=> nueve
					!in_array(intval(substr($numero_fono,2,1)),range(5,9)) ){	//3=> 5, 6, 7, 8 o 9
					//El resto de caracteres pueden ser cualquier número...
						//$fono_error	=1;
						return "0";
					} else {
						//$fono_error		= 0;
						return $numero_fono;
					}

				} else {
					//$fono_error		= 1;
					return "0";
				}

		} else { //Red fija otro caso
			$codigo_num =substr($numero_fono,0,2);
			if(strlen($numero_fono)==9 ){
				//A revisar reglas!, para santiago código es "22"
				if(substr($numero_fono,0,2) == "22") //Ya se cumple que las primeras 2 cifras sean 2 y 2 respectivamente, para RM
				{
					if( !in_array(intval(substr($numero_fono,2,1)),range(1,9)) ){	//3=> 1,2,3,4,5,6,7,8 o 9
						//$fono_error 	= 1;
						return "0";
					} else {
						//$fono_error		= 0;
						return $numero_fono;
					}
				} else  { //Cualquier otro digito se asume como regional
					if( !in_array(intval(substr($numero_fono,0,1)), range(3,7)) || //1=>3,4,5,6 o 7
						!in_array(intval(substr($numero_fono,1,1)), range(1,8)) || //2=>1,2,3,4,5,6,7, o el 8 
						!in_array(intval(substr($numero_fono,2,1)), range(1,9)) ){  //3=>1 al 9 no cero
						//$fono_error 	= 1;
						return "0";
					} else {
						//$fono_error		= 0;
						return $numero_fono;
					}
				}  //Si habrían más casos abrir un else más acá

			} else {
				//$fono_error = 1; //Claramente hay error por ser un número de longitud inferior
				return "0";
				// Longitudes admisibles hasta este punto: 09xxx xxxx=9 caracteres y YY xxx xxxx=9 caracteres
			}
		} 
    }
    
    // Función que busca formatear una fecha al formato de almacenado de la BD obtenida del modo dd-mm-yyyy
	private static function formateaFecha_($fecha) {
		try{
			$unix_date 					= ($fecha - 25569) * 86400;
			$excel_date 				= 25569 + ($unix_date / 86400);
			$unix_date 					= ($excel_date - 25569) * 86400;
			$date_ 						= gmdate("Y-m-d", $unix_date);
			return $date_;
		}
		catch(Exception $e){
			return false;
		}
		
    }
    
    //Función que formatea una fecha al formato de almacenado de la BD obtenida como texto, formato dd/mm/yyyy hh:mm. Sólo almacenará Y-m-d
	private static function formateaFechaLarga_($fecha) {
		try{

			$date_formateada = trim($fecha);
			$df = explode(" ",$date_formateada);
			$dfor = $df[0];
			$dfor = str_replace("/","-",$dfor);
			
			return date('Y-m-d',strtotime($dfor));
		}
		catch(Exception $e){
			return false;
		}
    }
    
    //Formateador de rut
	//La función siempre devolverá "-" al encontrarse con sólo strings. Esta función no valida, sólo formatea
	private static function formatea_rut_($rut)
	{
		$rut_formateado = trim($rut); //Eliminando espacios
		$rut_formateado = str_replace('.','',$rut_formateado); //Eliminando puntitos que pudiese tener
		$rut_formateado = str_replace('-','',$rut_formateado); //Eliminando guiones
		$rut_formateado = preg_replace('/[a-zA-ZáéíóúÁÉÍÓÚ]/i','',$rut_formateado); //Eliminando letras u otros
		//Ahora a trabajarlo más. A colocarlo de la forma XXXXXXX-X
		$dv = substr($rut_formateado,-1,1);
		$cuerpo = substr($rut_formateado,0,-1);
		$rut_formateado = $cuerpo."-".$dv;
		return $rut_formateado;

	}
	//Validador de rut
	private static function valida_rut_($rut)
	{
		$rut = self::formatea_rut_($rut);
		if (!preg_match("/^[0-9.]+[-]?+[0-9kK]{1}/", $rut)) {
			return false;
		}

		$rut = preg_replace('/[\.\-]/i', '', $rut);
		$dv = substr($rut, -1);
		$numero = substr($rut, 0, strlen($rut) - 1);
		$i = 2;
		$suma = 0;
		foreach (array_reverse(str_split($numero)) as $v) {
			if ($i == 8)
				$i = 2;
			$suma += $v * $i;
			++$i;
		}
		$dvr = 11 - ($suma % 11);

		if ($dvr == 11)
			$dvr = 0;
		if ($dvr == 10)
			$dvr = 'K';
		if ($dvr == strtoupper($dv))
			return true;
		else
			return false;
	}
	
	//Función que entrega un booleando.
	//Ésta revisa si un número telefónico se repite o no detro de una colección de datos, en un mismo rut
	//El número podría repetirse en otros rut
	private static function telefonoDuplicado_($coleccion, $rut, $fono){
		if(count($coleccion) != 0){
			$flag = true; //Bandera que contará si existe al menos, una vez el número repetido en la $coleccion
			foreach($coleccion as $c){
				if($c['rut']==$rut && $c['fono']==$fono) //Se encuetra el rut y el fono que ya existe, por tanto flag=False;
					$flag = false;
				if(!$flag)
					break; //Se rompe el foreach proque el número se ecuentra duplicado
			}
			return $flag; //True si no hay números dulplicados en el array y false si encuentra
		}else{
			return true; //Como no hay elementos en la colección, retornará true para que se inserte un número en la función completaArrayTelefonico
		}
	}
	
	//Función que permite generar un único array con los datos de rut, fono, empresa y la prioridad determinada
	private static function completaArrayTelefonico_($coleccion, $arreglo_lectura, $param_fono, $empresa, $prioridad){
		
		if($arreglo_lectura){ //si existe...
			foreach($arreglo_lectura as $array_excel){
			
				//->Paso 3.1 Se revisará si existe algún número repetido para un mismo rut 
				if(self::telefonoDuplicado_($coleccion, $array_excel['rut'], $array_excel[$param_fono]) && self::validaTelefono_($array_excel[$param_fono])!="0" && self::validaTelefono_($array_excel[$param_fono])!= "999999999"){
				//if(self::telefonoDuplicado_($coleccion, $array_excel['rut'], $array_excel[$param_fono])){	
					//->Paso 3.2 Se setean los números en la coleccion, por rut
					array_push($coleccion, array(
						'rut'		=>$array_excel['rut'],
						//'fono'		=>$array_excel[$param_fono],
						'fono'		=>self::validaTelefono_($array_excel[$param_fono]),
						'empresa'	=>$empresa,
						'prioridad'	=>$prioridad
					));
				}
				
			}
		}

		return $coleccion;
	}

	/* Función que comprueba si existen existencias o no en un arreglo leído, pero que no cuenta con teléfonos */

	private static function existenciaTelefonos_($arreglo_lectura, $param_fono){
		$contador_telefono = 0;
		foreach($arreglo_lectura as $array_excel){
			if($array_excel[$param_fono] != '' && $array_excel[$param_fono] != null)
				$contador_telefono+=1;
			if($contador_telefono >= 1)
				return true; //Hay números
		}
		return false; //Contador quedó en cero, no hay números en el array
	}

	/* 
	Función que efectúa una query y entrega un array de array de resultados
	*/
	private static function queryArray_($query){
		try{
			$con = Conexion::conectar();
			$respuesta = pg_query($con,$query);
			$array_respuesta=pg_fetch_all($respuesta);
			pg_free_result($respuesta);
			pg_close($con);
			return $array_respuesta;
		} catch(Exception $e){
			$array_respuesta = array(array(
				'rut'		=>null,
				'fono'		=>null,
				'empresa'	=>null
			));
			return $array_respuesta;
		}
	}

	private static function queryExists_($query){
		try{
			$con = Conexion::conectar();
			$respuesta = pg_query($con,$query);
			$array_respuesta=pg_fetch_assoc($respuesta);
			pg_free_result($respuesta);
			pg_close($con);
			return $array_respuesta;
		} catch(Exception $e){
			$array_respuesta = array('resultado'=>'f');
			return $array_respuesta;
		}
	}
	//Método que sirve para agrupar los arrays según el argumento especificado
	/*** @params $array = arreglo de arreglos
		@params $groupkey es la clave a agrupar, por ejemplo un rut
		@returns array 
		Ejemplo de uso:
		$arreglo = array(
			array(
					'name' => 'Juan',
					'color' => 'Azul',
					'edad' => 24
				),
			array
				(
					'name' => 'Juan',
					'color' => 'Rojo',
					'edad' => 24

				),
			array
				(
					'name' => 'Juan',
					'color' => 'Verde',
					'edad' => 24
				),
			array
				(
					'name' => 'Pablo',
					'color' => 'Amarillo',
					'edad' => 25
				)
			);
		$groupkey = 'name';
		print_r(groupArray($arreglo,$groupkey));
		Entrega: Una colección agrupada por nombre y en groupeddata, un arreglo con los otros argumentos, indexados numéricamente
		
		
		**/
	private static function groupArray_($array, $groupkey){
		if (count($array)>0){
		$keys = array_keys($array[0]);
		$removekey = array_search($groupkey, $keys);		
		if ($removekey===false)
			return array(0=>"Clave no existe");
		else
		unset($keys[$removekey]);
		$groupcriteria = array();
		$return=array();
		foreach($array as $value){
			$item=null;
			foreach ($keys as $key)
			{
				$item[$key] = $value[$key];
			}
			$busca = array_search($value[$groupkey], $groupcriteria);
			if ($busca === false)
			{
				$groupcriteria[]=$value[$groupkey];
				$return[]=array($groupkey=>$value[$groupkey],'groupeddata'=>array());
				$busca=count($return)-1;
			}
			$return[$busca]['groupeddata'][]=$item;
		}
		return $return;
	}
	else
		return array();
	}

	private static function txtCSV_($array, $groupkey,$arreglo_coleccion){
		//El array recibido debe provenir del método groupArray_($array, $groupkey). De hecho, se empleará ello para este método
		$arreglo = self::groupArray_($array, $groupkey);
		if($arreglo[0] == "Clave no existe")
			return false;
		else{
			$cadena_csv = "\"telefono\",\"rut\",\"nombre\"\r\n";
			//Recorrido al array $arreglo. Las respuestas esperadas son del tipo array([0]=>array($groupkey,array...),---)
			foreach($arreglo as $arr){
				$rut = $arr[$groupkey];
				$cadena_fono='';
				//Concatenando los números de teléfono
				foreach($arr['groupeddata'] as $phone){
					if($phone['fono']!=null || $phone['fono']!='')
						$cadena_fono .=$phone['fono']."-";
				}
				//Eliminando el último "-"
				$cadena_fono = substr($cadena_fono, 0, -1);
				//Ahora hay que recorrer el $arreglo_coleccion para hallar el key asociado al rut
				foreach($arreglo_coleccion as $ac){
					if(array_search($rut, $ac)==$groupkey){ //Buscando el rut dentro del arreglo de lectura
						$nombre = $ac['Nombre'];
						break;
					}
				}
				if(isset($nombre))
				//Todo hallado, se procede a la concatenación con $cadena_csv
					$cadena_csv .= $cadena_fono.","."\"".$rut."\"".","."\"".$nombre."\"\r\n";
				else
				$cadena_csv .= $cadena_fono.","."\"".$rut."\"".","."\""."ERROR"."\"\r\n";
			}
			return $cadena_csv;
		}
	}

	private static function envioCorreo_($array_destinatarios, $array_url_archivos, $cuerpo_mensaje){
			$mail = new PHPMailer();			
			$mail->isSMTP();            
			$mail->Host = "ssl://mail.2080.cl";
			$mail->Port = 465; 
			$mail->SMTPAuth = true;                           
			$mail->Username =  "cobra@2080.cl";                            
			$mail->Password = "N25VLHx.zM";                                                
			$mail->SMTPSecure = "SSl";                                                 			
			$mail->From = "cobra@2080.cl";
			$mail->FromName = "Adm.Cobra 2080 ";
			//Recorriendo a destinatarios
			foreach($array_destinatarios as $destinatario){
				$mail->addAddress($destinatario);
			}

			$mail->isHTML(true);
			//Recorriendo rutas de adjuntos
			if(count($array_url_archivos) > 0){
				foreach($array_url_archivos as $url){
					$mail->AddAttachment($url);
				} 			
			}
			$mail->Subject = "Campaña TCH: Carga de datos asociada al ".date('d-m-Y')." Finalizado.";				
			$mail->Body = $cuerpo_mensaje;
				
			try{
				if(!$mail->send()) {
					echo "Mailer Error: " . $mail->ErrorInfo;
				}else {
						
					echo 'Mensaje a sido enviado con exito.';				
				}
			}
			catch(phpmailerException $e) {
				echo $e->errorMessage(); //error messages from PHPMailer
				return false;
			} 
			catch (Exception $e) {
				echo $e->getMessage();
				return false;
		}
	}

	private static function insertTelefono_servicio_cURL_($rut, $origen, $telefono)
	{
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => "apicampanas.pagekite.me/api/telefono/nuevonumero",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS =>"{ \n\t\"rut\" : \"".$rut."\",\n\t\"origen\" : ".$origen.",\n\t\"numero_fono\" : \"".$telefono."\"\n}",
			CURLOPT_HTTPHEADER => array(
			  "Content-Type: application/json"
			),
		  ));

		$response = curl_exec($curl);

		curl_close($curl);
		return $response;
	}

	public static function insertTelefono_servicio_cURL($rut, $origen, $telefono){
		return self::insertTelefono_servicio_cURL_($rut, $origen, $telefono);
	}

    //Métodos públicos que garantizan el encapsulamiento
    public static function validateDate($date){
        return self::validateDate_($date);
    }
    public static function validaTelefono($fono){
        return self::validaTelefono_($fono);
    }
    public static function formateaFecha($date){
        return self::formateaFecha_($date);
    }
    public static function formateaFechaLarga($date){
        return self::formateaFechaLarga_($date);
    }
    public static function formatea_rut($rut){
        return self::formatea_rut_($rut);
    }
    public static function valida_rut($rut){
        return self::valida_rut_($rut);
	}
	public static function telefonoDuplicado($coleccion, $rut, $fono){
		return self::telefonoDuplicado_($coleccion, $rut, $fono);
	}
	public static function completaArrayTelefonico($coleccion, $arreglo_lectura, $param_fono, $empresa, $prioridad){
		return self::completaArrayTelefonico_($coleccion, $arreglo_lectura, $param_fono, $empresa, $prioridad);
	}
	public static function existenciaTelefonos($arreglo_lectura, $param_fono){
		return self::existenciaTelefonos_($arreglo_lectura, $param_fono);
	}

	/** Método que permite ejecutar una consulta en BD postgres. 
	** @param $query string (La consulta a ejecutar) 
	** returns array (Arreglo de arreglo de resultados, al estilo de pg_fetch_all)
	**/
	public static function queryArray($query){
		return self::queryArray_($query);
	}
	public static function queryExists($query){
		return self::queryExists_($query);
	}
	public static function groupArray($array, $groupkey){
		return self::groupArray_($array, $groupkey);
	}
	/** Método que permite generar un string para la generación de un futuro archivo csv
	** @param $array array (arreglo generado tras ejecutar las querys)
	** @param $groupkey string (el rótulo por el cual se ha de efectuar la agrupación.)
	** @param $coleccion array (es el arreglo leido desde un xlsx)  
	** returns string
	**/
	public static function txtCSV($array, $groupkey, $coleccion){
		return self::txtCSV_($array, $groupkey, $coleccion);
	}

	/**Envìa por correo electrònico uno o varios archivos a un o varios destinatarios. Los destinatarios son obligatorios
	 ** params @$array_destinatarios array (arreglo con correos del tipo alguien@ejemplo.com)
	 ** params @$array_url_archivos array (arreglo con url absolutas de archivos para ser adjuntadas al correo)
	 ** params @$cuerpo_mensaje string (código html con el mensaje del correo. Este campo es deseable pero opcional)
	 ** returns boolan (si lo envía: true; false en caso contrario)  
	 **/
	public static function envioCorreo($array_destinatarios, $array_url_archivos, $cuerpo_mensaje){
		return self::envioCorreo_($array_destinatarios, $array_url_archivos, $cuerpo_mensaje);
	}

	

}